from mshr import *
from dolfin import *
import numpy
from islandmesh_fromstl import highest_point, min_coord_i, dist
npa = numpy.array

# hei

stlfile = "Rektangel_simpel.stl"
g = Surface3D(stlfile)

# Create a mesh around the house
d = CSGCGALDomain3D(g)
v = d.get_vertices()

lenv = len(v)
vx = npa([v[i][0] for i in range(0, lenv)])
vy = npa([v[i][1] for i in range(0, lenv)])
vz = npa([v[i][2] for i in range(0, lenv)])

xmin = min(vx); xmax = max(vx)
ymin = min(vy); ymax = max(vy)
zmin = min(vz); zmax = max(vz)

hx = xmax - xmin
hy = ymax - ymin
hz = zmax - zmin 

bxmin = xmin - 1.5*hx; bxmax = xmax + 1.5*hx
bymin = ymin - 1.0*hy; bymax = ymax + 1.0*hy
bzmin = zmin - 1.0*hz; bzmax = zmax + 1.0*hz

surdom = Box(Point(bxmin, bymin, bzmin), Point(bxmax, bymax, bzmax))

mesh_house = generate_mesh(surdom - g, 15)

# Move the house to a location on the island
#correction_point = Point(-1.0*hx, -2.0*hy, 2.0*hz)
correction_point = Point(-1.0*hx, -2.0*hy, -0.65*hz)
house_location = highest_point + correction_point
mesh_house.translate(house_location)

# Normalize the house mesh in relation to the island mesh
xs = mesh_house.coordinates()

for i, x in enumerate(xs):
    xs[i][0] = (x[0] - min_coord_i[0]) / dist
    xs[i][1] = (x[1] - min_coord_i[1]) / dist
    xs[i][2] = (x[2] - min_coord_i[2]) / dist

# Save the mesh
File("meshes/mesh_house.xml") << mesh_house
File("meshes/mesh_house.pvd") << mesh_house  
