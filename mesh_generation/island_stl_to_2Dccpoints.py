from fenics import *
from mshr import *
import numpy as np
import sys

# Turn data from stl-file into something 2D fenics friendly

# Load stl-file
stlfile = "../stl-files/island_enkel-A.stl"
g = Surface3D(stlfile)
d0 = CSGCGALDomain3D(g)
v0 = d0.get_vertices()
d = d0.convex_hull(v0)
v = d.get_vertices()

lenv = len(v)
vx = np.array([v[i][0] for i in range(lenv)])
vy = np.array([v[i][1] for i in range(lenv)])
vz = np.array([v[i][2] for i in range(lenv)])

x_imin = min(vx); x_imax = max(vx)
y_imin = min(vy); y_imax = max(vy)
z_imin = min(vz); z_imax = max(vz)

l_ix = x_imax - x_imin
l_iy = y_imax - y_imin

extra_fac = 0.1
x_amin = x_imin - extra_fac*l_ix; x_amax = x_imax + extra_fac*l_ix
y_amin = y_imin - extra_fac*l_iy; y_amax = y_imax + extra_fac*l_iy

lenzmin = 0
vx_zmin = np.array([]); vy_zmin = np.array([])
for i in range(lenv):
    if vz[i] == z_imin:
        lenzmin += 1
        vx_zmin = np.append(vx_zmin, vx[i])
        vy_zmin = np.append(vy_zmin, vy[i])

island_bottom = np.array([np.array([vx_zmin[i], vy_zmin[i]]) for i in range(lenzmin)])
np.save('plot2D/island_bottom.npy', island_bottom)
