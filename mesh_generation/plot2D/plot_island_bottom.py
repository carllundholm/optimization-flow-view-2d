import matplotlib.pyplot as plt
import numpy as np
from math import *
import sys

# Load data
arr = np.load('island_bottom.npy')
a1 = len(arr)
a0 = 3
print a1, a0
x0 = [arr[i][0] for i in range(0, a0)]
y0 = [arr[i][1] for i in range(0, a0)]
x1 = [arr[i][0] for i in range(a0, a1)]
y1 = [arr[i][1] for i in range(a0, a1)]

#plt.plot(x0, y0, 'ob')
#plt.plot(x1, y1, 'o-')
#plt.show()

x = x1; y = y1
xlen = len(x)
xmid = sum(x)/xlen
ymid = sum(y)/xlen

x_n = x - xmid; y_n = y - ymid
angles = []
for i in range(xlen):
    xx = x_n[i]; yy = y_n[i]
    r = sqrt(xx**2 + yy**2)
    if yy >= 0:
        if xx >= 0:
            angles.append(np.arcsin(yy / r))
        else:
            angles.append(np.arcsin(-xx / r) + 0.5*pi)
    else:
        if xx >= 0:
            angles.append(np.arcsin(xx / r) + 1.5*pi)
        else:
            angles.append(np.arcsin(-yy / r) + pi)

x_cc = []; y_cc = []
points_cc = []
angles_sorted = sorted(angles);
angle_diff = 0.05*pi
angle_old = angles_sorted[0] - 2 * angle_diff
for angle in angles_sorted:
    if angle - angle_old > angle_diff:
        i = np.where(angles == angle)[0][0]
        points_cc.append([x[i], y[i]])
        angle_old = angle

        #x_cc.append(x[i])
        #y_cc.append(y[i])
        #plt.plot(x1, y1, 'o')
        #plt.plot(xmid, ymid, '*g')
        #plt.plot(x_cc, y_cc, 'or-')
        #plt.show()

np.save('points_cc.npy', points_cc)

#plt.plot(x1, y1, 'o')
#plt.plot(xmid, ymid, '*g')
#plt.plot(np.append(x_cc, x_cc[0]), np.append(y_cc, y_cc[0]), 'or-')
#plt.show()
