from fenics import *
from mshr import *
import numpy as np
import sys

### Generate air mesh ###
# Turn data from stl-file into something 2D fenics friendly
stlfile = "../stl-files/island_enkel-A.stl"
g = Surface3D(stlfile)
d0 = CSGCGALDomain3D(g)
v0 = d0.get_vertices()
d = d0.convex_hull(v0)
v = d.get_vertices()

lenv = len(v)
vx = np.array([v[i][0] for i in range(lenv)])
vy = np.array([v[i][1] for i in range(lenv)])
vz = np.array([v[i][2] for i in range(lenv)])

x_imin = min(vx); x_imax = max(vx)
y_imin = min(vy); y_imax = max(vy)
z_imin = min(vz); z_imax = max(vz)

l_ix = x_imax - x_imin
l_iy = y_imax - y_imin

extra_fac = 0.1
x_amin = x_imin - extra_fac*l_ix; x_amax = x_imax + extra_fac*l_ix
y_amin = y_imin - extra_fac*l_iy; y_amax = y_imax + extra_fac*l_iy

domain_air = Rectangle(Point(x_amin, y_amin), Point(x_amax, y_amax))

lenzmin = 0
vx_zmin = np.array([]); vy_zmin = np.array([])
for i in range(lenv):
    if vz[i] == z_imin:
        lenzmin += 1
        vx_zmin = np.append(vx_zmin, vx[i])
        vy_zmin = np.append(vy_zmin, vy[i])

island_bottom = np.array([np.array([vx_zmin[i], vy_zmin[i]]) for i in range(lenzmin)])
np.save('plot2D/island_bottom.npy', island_bottom)
pcc = np.load('plot2D/points_cc.npy')

island_bottom_Points_cc = []
for i in range(len(pcc)):
    island_bottom_Points_cc.append(Point(pcc[i][0], pcc[i][1]))

# Create 2D island domain
domain_island = Polygon(island_bottom_Points_cc)
domain_air.set_subdomain(1, domain_island)

# Generate air mesh
mesh_air = generate_mesh(domain_air, 60)

# Mark the island in air mesh
island_markers = MeshFunction("size_t", mesh_air, 1)
island_markers.set_all(1)
for f in facets(mesh_air):
    if domain_island.inside(f.midpoint()):
        island_markers.set_value(f.index(), 0)

# Save the island markers
File("../main/meshes/island_markers.xml") << island_markers
File("../main/meshes_pvd/island_markers.pvd") << island_markers

# Normalize the mesh
xs = mesh_air.coordinates()
xmin = min([x[0] for x in xs])
xmax = max([x[0] for x in xs])
ymin = min([x[1] for x in xs])
ymax = max([x[1] for x in xs])

dist = max(xmax - xmin, ymax - ymin)

for i, x in enumerate(xs):
    xs[i][0] = (x[0] - xmin) / dist
    xs[i][1] = (x[1] - ymin) / dist

# Save the mesh
File("../main/meshes/mesh_air.xml") << mesh_air
File("../main/meshes_pvd/mesh_air.pvd") << mesh_air

# Normalize the counter clockwise island boundary points
island_boundary_cc = np.array([np.zeros([2])]*len(island_bottom_Points_cc))
for i, x in enumerate(island_bottom_Points_cc):
    island_boundary_cc[i][0] = (x[0] - xmin) / dist
    island_boundary_cc[i][1] = (x[1] - ymin) / dist

# Save the normalized counter clockwise island boundary points
np.save('../main/meshes/island_boundary_cc.npy', island_boundary_cc)

### Generate housebox mesh ###
# Create house domain
l_hx = 0.05; l_hy = 0.5 * l_hx
x_hmin = -0.5*l_hx; y_hmin = -0.5*l_hy;
x_hmax = 0.5*l_hx; y_hmax = 0.5*l_hy
pi_fac = l_hx*2e-2*pi
p_h0 = Point(x_hmin, y_hmin) - pi_fac*Point(1.0, 1.0)
p_h1 = Point(x_hmax, y_hmax) + pi_fac*Point(1.0, 1.0)
domain_house = Rectangle(p_h0, p_h1)
#eps = 1e-2 * Point(1.0, 1.0)
#domain_house_interior = Rectangle(p_h0 + eps, p_h1 - eps)

# Save house dimensions for later use in view computations
h_dim_x = domain_house.second_corner().x() - domain_house.first_corner().x()
h_dim_y = domain_house.second_corner().y() - domain_house.first_corner().y()
np.save('../main/meshes/house_dimensions.npy', np.array([h_dim_x, h_dim_y]))

# Create housebox domain
#lb = 10*pi_fac
lb = (2.0 + (pi - 3.0))*mesh_air.hmax()
r = 0.5*sqrt(l_hx**2 + l_hy**2)
R = 2*r
#D = r + R
#domain_housebox = Circle(Point(0.0, 0.0), R)
0.05#domain_housebox = Ellipse(Point(0.0, 0.0), 0.6*l_hx + lb, 0.6*l_hy + lb)
#domain_housebox = Rectangle(-R*Point(1.0, 1.0), -R*Point(1.0, 1.0))
domain_housebox = Rectangle(p_h0 - Point(lb, lb), p_h1 + Point(lb, lb))
#R = sqrt((0.5*l_hx + lb)**2 + (0.5*l_hy + lb)**2)

# Generate housebox mesh
#domain_housebox.set_subdomain(1, domain_house)
domain_housebox -= domain_house
#domain_housebox.set_subdomain(2, domain_house_interior)
mesh_hb_init = generate_mesh(domain_housebox, 16)

# Define house boundary
def on_house_boundary(x):
    if (near(x[0], p_h0[0]) and p_h0[1] <= x[1] and x[1] <= p_h1[1])  or \
       (near(x[0], p_h1[0]) and p_h0[1] <= x[1] and x[1] <= p_h1[1])  or \
       (near(x[1], p_h0[1]) and p_h0[0] <= x[0] and x[0] <= p_h1[0])  or \
       (near(x[1], p_h1[1]) and p_h0[0] <= x[0] and x[0] <= p_h1[0]):
        return True
    else:
        return False

# Save the mesh
File("../main/meshes_pvd/mesh_hb_init.pvd") << mesh_hb_init
File("../main/meshes/mesh_hb_init.xml") << mesh_hb_init

# Mark the house in housebox mesh
house_markers = MeshFunction("size_t", mesh_hb_init, 1)
house_boundary_markers = MeshFunction("size_t", mesh_hb_init, 1)
house_markers.set_all(0)
house_boundary_markers.set_all(0)
for f in facets(mesh_hb_init):
    if domain_house.inside(f.midpoint()):
        house_markers.set_value(f.index(), 1)
        if on_house_boundary(f.midpoint()):
            house_boundary_markers.set_value(f.index(), 1)
File("../main/meshes_pvd/house_markers.pvd") << house_markers
File("../main/meshes/house_markers.xml") << house_markers
File("../main/meshes_pvd/house_boundary_markers.pvd") << house_boundary_markers
File("../main/meshes/house_boundary_markers.xml") << house_boundary_markers

### Generate and save meshes for visualization
# Island mesh for visualization
island_boundary_cc_Points = []
for p in island_boundary_cc:
    island_boundary_cc_Points.append(Point(p[0], p[1]))
mesh_island = generate_mesh(Polygon(island_boundary_cc_Points), 1)
File("../main/meshes_pvd/mesh_island.pvd") << mesh_island

# House mesh for visualization
mesh_house_init = generate_mesh(domain_house, 1)
File("../main/meshes_pvd/mesh_house_init.pvd") << mesh_house_init
File("../main/meshes/mesh_house_init.xml") << mesh_house_init
