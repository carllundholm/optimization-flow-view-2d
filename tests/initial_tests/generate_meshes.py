from fenics import *
from mshr import *

### Generate meshes ###
# Parameters for air mesh
x_amin = 0.0; y_amin = 0.0;
x_amax = 1.0; y_amax = 0.5
p_a0 = Point(x_amin, y_amin)
p_a1 = Point(x_amax, y_amax)
n_ax = 40; n_ay = 20
mesh_air = RectangleMesh(p_a0, p_a1, n_ax, n_ay)

# Save to file
File("meshes/mesh_air.xml") << mesh_air

# Parameters for house mesh(es)
x_hmin = -0.05; y_hmin = -0.025;
x_hmax = 0.05; y_hmax = 0.025
p_h0 = Point(x_hmin, y_hmin) - 2e-3*Point(pi, pi)
p_h1 = Point(x_hmax, y_hmax) + 2e-3*Point(pi, pi)
house = Rectangle(p_h0, p_h1)

l_hx = x_hmax - x_hmin; l_hy = y_hmax - y_hmin
lb = 2e-2*pi
r = 0.5*sqrt(l_hx**2 + l_hy**2)
#R = 2*r
#D = r + R
#housebox = Circle(Point(0.0, 0.0), R)
#housebox = Ellipse(Point(0.0, 0.0), 0.6*l_hx + lb, 0.6*l_hy + lb)
#housebox = Rectangle(-R*Point(1.0, 1.0), -R*Point(1.0, 1.0))
housebox = Rectangle(p_h0 - Point(lb, lb), p_h1 + Point(lb, lb))
R = sqrt((0.5*l_hx + lb)**2 + (0.5*l_hy + lb)**2)

housebox.set_subdomain(1, house)
mesh_hb_init = generate_mesh(housebox, 8)

File("meshes/mesh_hb_init.pvd") << mesh_hb_init
File("meshes/mesh_hb_init.xml") << mesh_hb_init

# Mark the house in housebox mesh
house_markers = FacetFunction("uint", mesh_hb_init)
house_markers.set_all(0)
for f in facets(mesh_hb_init):
    if house.inside(f.midpoint()):
        house_markers.set_value(f.index(), 1)

File("meshes/house_markers.xml") << house_markers
