from fenics import *
import numpy as np
import sys

### Load and place meshes ###
# Load air mesh
mesh_air = Mesh("meshes/mesh_air.xml")
File("meshes/mesh_air.pvd") << mesh_air

mac = mesh_air.coordinates()
mac_x = np.array([]); mac_y = np.array([])
for coord in mac:
    mac_x = np.append(mac_x, coord[0])
    mac_y = np.append(mac_y, coord[1])

x_amin = min(mac_x); y_amin = min(mac_y)
x_amax = max(mac_x); y_amax = max(mac_y)

p_a0 = Point(x_amin, y_amin)
p_a1 = Point(x_amax, y_amax)

# Initialize multimesh and add air mesh
multimesh = MultiMesh()
multimesh.add(mesh_air)

# Parameters for house circle
numh = 3
numh_ghost = 0
if numh > 0:
    circmid = 0.5 * (p_a0 + p_a1) #- Point(0.0, 0.25*(y_amax - y_amin))
    circradius = Point(0.0, 0.31*(y_amax - y_amin))
    theta = 360 / (numh + numh_ghost)
    theta_rad = 2 * pi * theta / 360

    # Place house(s) in circle
    mesh_housebox = []
    for hb in range(numh):
        mesh_hb = Mesh("meshes/mesh_hb_init.xml")

        # Rotate house
        angle = (hb + numh_ghost) * theta #+ 108
        mesh_hb.rotate(angle, 2)

        # Translate house
        angle_rad = (hb + numh_ghost) * theta_rad #+ 0.6*pi
        rotvec = circradius.rotate(Point(0, 0, 1), angle_rad)
        trans_point = circmid + rotvec;
        mesh_hb.translate(trans_point)

        # Save to file and add to multimesh hierarchy
        mesh_housebox.append(mesh_hb)
        File("meshes/mesh_hb"+str(hb)+".pvd") << mesh_hb
        multimesh.add(mesh_hb)

# Build multimesh hierarchy
multimesh.build()

### Define additional problem data ###
# Define source term
f = Constant((0.0, 0.0))

# Define boundary regions
class XminBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], x_amin)

class XmaxBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], x_amax)

class YBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (near(x[0], x_amin) or
                                near(x[0], x_amax))

class YminBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], y_amin)

class YmaxBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], y_amax)

class XBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (near(x[1], y_amin) or
                                near(x[1], y_amax))


### Define and solve FE-variational formulation ###
# Create function space
P2 = VectorElement("P", triangle, 2)
P1 = FiniteElement("P", triangle, 1)
TH = P2 * P1
W = MultiMeshFunctionSpace(multimesh, TH)

# Define trial and test functions
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)

# Define normal and mesh size
n = FacetNormal(multimesh)
h = 2.0 * Circumradius(multimesh)

# Parameters and functions for bilinear form
alpha = Constant(1e1)
beta = Constant(1.0)

def tensor_jump(v, n):
    return outer(v('+'), n('+')) + outer(v('-'), n('-'))

def a_h(v, w):
    return inner(grad(v), grad(w)) * dX \
         - inner(avg(grad(v)), tensor_jump(w, n)) * dI \
         - inner(avg(grad(w)), tensor_jump(v, n)) * dI \
         + alpha/avg(h) * inner(jump(v), jump(w)) * dI

def b_h(v, q):
    return -div(v) * q * dX + jump(v, n) * avg(q) * dI

def s_O(v, w):
    return beta * inner(jump(grad(v)), jump(grad(w))) * dO

def s_C(v, q, w, r):
    return h*h * inner(-div(grad(v)) + grad(q), -div(grad(w)) - grad(r)) * dC

# Functions for linear form
def l_h(f, v):
    return inner(f, v) * dX

def l_C(f, v, q):
    return h*h * inner(f, -div(grad(v)) - grad(q)) * dC

# Define bilinear form
a = a_h(u, v) + b_h(v, p) + b_h(u, q) + s_O(u, v) + s_C(u, p, v, q)

# Define linear form
L =  l_h(f, v) + l_C(f, v, q)

# Assemble linear system
A = assemble_multimesh(a)
b = assemble_multimesh(L)

# Define boundary values
inflow_value = Constant((0.0, 1.0))
outflow_value = Constant(0.0)
bulk_value = Constant((0.0, 1.0))
noslip_value = Constant((0.0, 0.0))

# Create subdomains for boundary conditions
inflow_boundary = YminBoundary()
outflow_boundary = YmaxBoundary()
bulk_boundary = YBoundary()

# Create subspaces for boundary conditions
V = MultiMeshSubSpace(W, 0)
Q = MultiMeshSubSpace(W, 1)

# Create boundary conditions
bc_in = MultiMeshDirichletBC(V, inflow_value, inflow_boundary)
bc_out = MultiMeshDirichletBC(Q, outflow_value, outflow_boundary)
bc_bulk = MultiMeshDirichletBC(V, bulk_value, bulk_boundary)

# Apply boundary conditions
bc_in.apply(A, b)
bc_out.apply(A, b)
bc_bulk.apply(A, b)

# Set noslip on the house(s)
if numh > 0:
    for hb in range(numh):
        ff = MeshFunction("size_t", mesh_housebox[hb], "meshes/house_markers.xml")
        File("meshes/house_marker"+str(hb)+".pvd") << ff
        house_noslip = MultiMeshDirichletBC(V, noslip_value, ff, 1, hb + 1)
        house_noslip.apply(A, b)

W.lock_inactive_dofs(A, b)

# Compute solution
w = MultiMeshFunction(W)
solve(A, w.vector(), b)

# Save solution to file
u_air = w.part(0).sub(0)
p_air = w.part(0).sub(1)
File("solutions/u_air.pvd") << u_air
File("solutions/p_air.pvd") << p_air

if numh > 0:
    for hb in range(numh):
        u_hb = w.part(hb + 1).sub(0)
        p_hb = w.part(hb + 1).sub(1)

        File("solutions/u_hb"+str(hb)+".pvd") << u_hb
        File("solutions/p_hb"+str(hb)+".pvd") << p_hb
