from fenics import *

class mumps2D:

    # Create files for storing meshes and solutions
    @classmethod
    def init_outfiles(cls, numom):
        cls.outfile_u_bm = File("solutions/u_poisson_bm.pvd")
        cls.outfile_u_om = []
        for m in range(numom):
            cls.outfile_u_om.append(File("solutions/u_poisson_om"+str(m)+".pvd"))

    # Load background mesh
    @classmethod
    def load_background_mesh(cls):
        cls.mesh_background = Mesh("meshes/mesh_bm.xml")

        # Get background mesh min and max coordinates
        xs = cls.mesh_background.coordinates()
        cls.xbm_min = min([x[0] for x in xs])
        cls.xbm_max = max([x[0] for x in xs])
        cls.ybm_min = min([x[1] for x in xs])
        cls.ybm_max = max([x[1] for x in xs])

    # Place overlapping meshes and create multimesh hierarchy
    @classmethod
    def place_overlapping_meshes(cls, mesh_positions):

        # Initialize multimesh and first add background mesh
        cls.multimesh = MultiMesh()
        cls.multimesh.add(cls.mesh_background)

        # Place overlapping meshes
        cls.numom = len(mesh_positions)
        cls.meshes_overlapping = []
        for m in range(cls.numom):
            mesh_om = Mesh("meshes/mesh_om.xml")

            # Rotate house
            angle = mesh_positions[m][2]
            mesh_om.rotate(angle, 2)

            # Translate house
            trans_point = Point(mesh_positions[m][0], mesh_positions[m][1])
            mesh_om.translate(trans_point)

            # Add to multimesh hierarchy
            cls.meshes_overlapping.append(mesh_om)
            cls.multimesh.add(mesh_om)

        # Build multimesh hierarchy
        cls.multimesh.build()

    # Save house meshes to file
    @classmethod
    def save_overlapping_meshes(cls):
        for m in range(cls.numom):
            cls.outfile_mesh_house[m] << cls.meshes_overlapping[m]

    # Clear multimesh hierarchy
    @classmethod
    def clear_meshes(cls):
        cls.multimesh.clear()

    # Set the predefined problem data
    @classmethod
    def set_problem_data(cls):
        # Define source term
        f = Constant(1.0)

        # Define boundary regions
        class XminBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[0], cls.xbm_min)

        class XmaxBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[0], cls.xbm_max)

        class YBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (near(x[0], cls.xbm_min) or
                                        near(x[0], cls.xbm_max))

        class YminBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[1], cls.ybm_min)

        class YmaxBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[1], cls.ybm_max)

        class XBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (near(x[1], cls.ybm_min) or
                                        near(x[1], cls.ybm_max))

        class Boundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary

        ### Define FE-variational formulation ###
        # Create function space
        cls.V = MultiMeshFunctionSpace(cls.multimesh, "P", 1)

        # Define trial and test functions
        u = TrialFunction(cls.V)
        v = TestFunction(cls.V)

        # Define normal and mesh size
        n = FacetNormal(cls.multimesh)
        h = 2.0 * Circumradius(cls.multimesh)
        h = (h('+') + h('-')) / 2

        # Parameters and functions for bilinear form
        alpha = 10.0
        beta = 1.0

        # Define bilinear form
        a = dot(grad(u), grad(v))*dX \
          - dot(avg(grad(u)), jump(v, n))*dI \
          - dot(avg(grad(v)), jump(u, n))*dI \
          + alpha/h * jump(u)*jump(v)*dI \
          + beta/h**2 * dot(jump(u), jump(v))*dO

        # Define linear form
        L = f*v*dX

        # Assemble linear system
        cls.A = assemble_multimesh(a)
        cls.b = assemble_multimesh(L)

        # Define boundary values
        cls.homoD_value = Constant(0.0)

        # Create subdomains for boundary conditions
        boundary = Boundary()

        # Create boundary conditions
        bc = MultiMeshDirichletBC(cls.V, cls.homoD_value, boundary)

        # Apply boundary conditions
        bc.apply(cls.A, cls.b)

        # Set inactive dofs to zero
        cls.V.lock_inactive_dofs(cls.A, cls.b)

    @classmethod
    def solve_problem(cls):
        # Compute solution
        cls.u = MultiMeshFunction(cls.V)
        solve(cls.A, cls.u.vector(), cls.b)

    @classmethod
    def save_solution(cls):
        # Save solution to file
        u_bm = cls.u.part(0)
        cls.outfile_u_bm << u_bm

        for m in range(cls.numom):
            u_om = cls.u.part(m + 1)
            cls.outfile_u_om[m] << u_om

    @classmethod
    def run_mumps2D(cls, mesh_positions):
        # Place overlapping meshes
        cls.place_overlapping_meshes(mesh_positions)
        print("Background mesh set")
        print(cls.numom, "additional overlapping meshes set")

        # Save overlapping meshes
        #cls.save_house_meshes()

        # Define problem
        print("Setting Poisson problem data ...")
        cls.set_problem_data()
        print("Poisson problem data set")

        # Solve problem
        print("Solving Poisson problem ...")
        cls.solve_problem()
        print("Poisson problem solved")

        # Save solution
        cls.save_solution()

        # Clear multimesh hierarchy
        cls.clear_meshes()
        print("multimesh hierarchy cleared")
