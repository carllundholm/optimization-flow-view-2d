from fenics import *

class mumss2D:

    # Create files for storing meshes and solutions
    @classmethod
    def init_outfiles(cls, numom):
        cls.outfile_u_bm = File("solutions/u_bm.pvd")
        cls.outfile_p_bm = File("solutions/p_bm.pvd")
        if numom > 0:
            cls.outfile_mesh_om = []
            cls.outfile_u_om = []
            cls.outfile_p_om = []
            for m in range(numom):
                cls.outfile_mesh_om.append(File("meshes_pvd/mesh_om"+str(m)+".pvd"))
                cls.outfile_u_om.append(File("solutions/u_om"+str(m)+".pvd"))
                cls.outfile_p_om.append(File("solutions/p_om"+str(m)+".pvd"))

    # Load background mesh
    @classmethod
    def load_background_mesh(cls, mesh_name):
        cls.mesh_background = Mesh("meshes/"+mesh_name+".xml")
        File("meshes_pvd/mesh_bm.pvd") << cls.mesh_background

        # Get air mesh min and max coordinates
        xs = cls.mesh_background.coordinates()
        cls.xbm_min = min([x[0] for x in xs])
        cls.xbm_max = max([x[0] for x in xs])
        cls.ybm_min = min([x[1] for x in xs])
        cls.ybm_max = max([x[1] for x in xs])

    # Place overlapping meshes and create multimesh hierarchy
    @classmethod
    def place_overlapping_meshes(cls, mesh_names, mesh_positions):

        # Initialize multimesh and first add background mesh
        cls.multimesh = MultiMesh()
        cls.multimesh.add(cls.mesh_background)

        # Place houses
        cls.numom = len(mesh_positions)
        if cls.numom > 0:
            cls.meshes_overlapping = []
            for m in range(cls.numom):
                mesh_om = Mesh("meshes/"+mesh_names[m % len(mesh_names)]+".xml")

                # Rotate house
                angle = mesh_positions[m][2]
                mesh_om.rotate(angle, 2)

                # Translate house
                trans_point = Point(mesh_positions[m][0], mesh_positions[m][1])
                mesh_om.translate(trans_point)

                # Add to multimesh hierarchy
                cls.meshes_overlapping.append(mesh_om)
                cls.multimesh.add(mesh_om)

        # Build multimesh hierarchy
        cls.multimesh.build()

    # Save house meshes to file
    @classmethod
    def save_overlapping_meshes(cls):
        if cls.numom > 0:
            for m in range(cls.numom):
                cls.outfile_mesh_om[m] << cls.meshes_overlapping[m]

    # Clear multimesh hierarchy
    @classmethod
    def clear_meshes(cls):
        cls.multimesh.clear()

    # Set the predefined problem data
    @classmethod
    def set_problem_data(cls):
        # Define source term
        f = Constant((0.0, 0.0))

        # Define boundary regions
        class XminBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[0], cls.xbm_min)

        class XmaxBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[0], cls.xbm_max)

        class YBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (near(x[0], cls.xbm_min) or
                                        near(x[0], cls.xbm_max))

        class YminBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[1], cls.ybm_min)

        class YmaxBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[1], cls.ybm_max)

        class XBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (near(x[1], cls.ybm_min) or
                                        near(x[1], cls.ybm_max))

        ### Define FE-variational formulation ###
        # Create function space
        P2 = VectorElement("P", triangle, 2)
        P1 = FiniteElement("P", triangle, 1)
        TH = P2 * P1
        cls.W = MultiMeshFunctionSpace(cls.multimesh, TH)

        # Define trial and test functions
        (u, p) = TrialFunctions(cls.W)
        (v, q) = TestFunctions(cls.W)

        # Define normal and mesh size
        n = FacetNormal(cls.multimesh)
        h = 2.0 * Circumradius(cls.multimesh)

        # Parameters and functions for bilinear form
        alpha = Constant(1e1)
        beta = Constant(1e1)
        gamma = Constant(1e0)

        def tensor_jump(v, n):
            return outer(v('+'), n('+')) + outer(v('-'), n('-'))

        def a_h(v, w):
            return inner(grad(v), grad(w)) * dX \
                 - inner(avg(grad(v)), tensor_jump(w, n)) * dI \
                 - inner(avg(grad(w)), tensor_jump(v, n)) * dI \
                 + alpha/avg(h) * inner(jump(v), jump(w)) * dI

        def b_h(v, q):
            return -div(v) * q * dX + jump(v, n) * avg(q) * dI

        #def s_O(v, w):
        #    return beta * inner(jump(grad(v)), jump(grad(w))) * dO

        def s_O(v, w):
            return (beta / avg(h)**2) * inner(jump(v), jump(w)) * dO

        def s_P(q, r):
            return gamma * inner(jump(q), jump(r)) * dO

        def s_C(v, q, w, r):
            return h*h * inner(div(grad(v)) - grad(q), div(grad(w)) + grad(r)) * dC

        # Functions for linear form
        def l_h(f, v):
            return inner(f, v) * dX

        def l_C(f, v, q):
            return -h*h * inner(f, div(grad(v)) + grad(q)) * dC

        # Define bilinear form
        #a = a_h(u, v) + b_h(u, q) + b_h(v, p) + s_O(u, v) + s_C(u, p, v, q)
        a = a_h(u, v) + b_h(u, q) + b_h(v, p) + s_O(u, v) + s_C(u, p, v, q) + s_P(p, q)

        # Define linear form
        L = l_h(f, v) + l_C(f, v, q)

        # Assemble linear system
        cls.A = assemble_multimesh(a)
        cls.b = assemble_multimesh(L)

        # Define boundary values
        cls.inflow_value = Constant((2.0/sqrt(5.0), 1.0/sqrt(5.0)))
        cls.outflow_value = Constant(0.0)
        cls.noslip_value = Constant((0.0, 0.0))

        # Create subdomains for boundary conditions
        xmin_boundary = XminBoundary()
        xmax_boundary = XmaxBoundary()
        ymin_boundary = YminBoundary()
        ymax_boundary = YmaxBoundary()

        # Create subspaces for boundary conditions
        cls.V = MultiMeshSubSpace(cls.W, 0)
        cls.Q = MultiMeshSubSpace(cls.W, 1)

        # Create boundary conditions
        bc_xmin = MultiMeshDirichletBC(cls.V, cls.inflow_value, xmin_boundary)
        bc_xmax = MultiMeshDirichletBC(cls.Q, cls.outflow_value, xmax_boundary)
        bc_ymin = MultiMeshDirichletBC(cls.V, cls.inflow_value, ymin_boundary)
        bc_ymax = MultiMeshDirichletBC(cls.Q, cls.outflow_value, ymax_boundary)

        # Apply boundary conditions
        bc_xmin.apply(cls.A, cls.b)
        bc_xmax.apply(cls.A, cls.b)
        bc_ymin.apply(cls.A, cls.b)
        bc_ymax.apply(cls.A, cls.b)

        # Set inactive dofs to zero
        cls.W.lock_inactive_dofs(cls.A, cls.b)

    @classmethod
    def solve_problem(cls):
        # Compute solution
        cls.w = MultiMeshFunction(cls.W)
        solve(cls.A, cls.w.vector(), cls.b)

    @classmethod
    def compute_maxspeed_component(cls):
        # Compute solution
        cls.w = MultiMeshFunction(cls.W)
        solve(cls.A, cls.w.vector(), cls.b)

        # Compute max speed component
        maxspeedcomp = 0.0
        for p in range(1 + cls.numom):
            speedcomp = max(abs(cls.w.part(p).sub(0).compute_vertex_values()))
            if speedcomp > maxspeedcomp:
                maxspeedcomp = speedcomp

        return maxspeedcomp

    @classmethod
    def save_solution(cls):
        # Save solution to file
        u_bm = cls.w.part(0).sub(0)
        p_bm = cls.w.part(0).sub(1)
        cls.outfile_u_bm << u_bm
        cls.outfile_p_bm << p_bm

        if cls.numom > 0:
            for m in range(cls.numom):
                u_om = cls.w.part(m + 1).sub(0)
                p_om = cls.w.part(m + 1).sub(1)

                cls.outfile_u_om[m] << u_om
                cls.outfile_p_om[m] << p_om

    @classmethod
    def run_mumss2D(cls, mesh_names, mesh_positions):
        # Place overlapping meshes
        cls.place_overlapping_meshes(mesh_names, mesh_positions)
        print("Background mesh set")
        print(cls.numom, "additional overlapping meshes set")

        # Save house meshes
        #cls.save_house_meshes()

        # Define problem
        print("Setting problem data ...")
        cls.set_problem_data()
        print("Problem data set")

        # Solve problem
        print("Solving problem ...")
        cls.solve_problem()
        print("Problem solved")

        # Save solution
        cls.save_solution()

        # Clear multimesh hierarchy
        cls.clear_meshes()
        print("multimesh hierarchy cleared")

    @classmethod
    def debug_mumss2D(cls, mesh_names, mesh_positions, criticalspeed):
        # Set house meshes
        cls.place_overlapping_meshes(mesh_names, mesh_positions)

        # Define problem
        cls.set_problem_data()

        # Solve problem and compute max speed component
        maxspeedcomp = cls.compute_maxspeed_component()
        maxspeed = maxspeedcomp
        print("Max speed test = ", maxspeed)

        # Check if critical speed has been reached
        if  maxspeed > criticalspeed: #critical_speed:
            print("WARNING! Max speed is above critical speed of", criticalspeed)

            # Save overlapping meshes
            #cls.save_overlapping_meshes()

            # Save solution
            cls.save_solution()

            return False

        # Clear multimesh hierarchy
        cls.clear_meshes()

        return True
