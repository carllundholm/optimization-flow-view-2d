from fenics import *
import random as rm
import sys
from multimesh_stokes_2D import mumss2D
from multimesh_poisson_2D import mumps2D

# run_main() is called at the end.
# Choose termination criteria for running many problems
def run_main():
    #run_fixed_number_of_problems(numom, mesh_names, num_problems)
    run_until_criticalspeed_reached(numom, mesh_names, criticalspeed, max_num_problems)
    #run_poisson_problem(numom)

def generate_meshes():
    mesh_name = "mesh_bm"
    mesh_us = RectangleMesh(Point(0.0, 0.0), Point(2.0, 1.0), 8, 8)
    File("meshes/"+mesh_name+".xml") << mesh_us

    mesh_name = "mesh_om"
    mesh_us_scaled = UnitSquareMesh(4, 4)
    mesh_us_scaled.scale(0.3)
    File("meshes/"+mesh_name+".xml") << mesh_us_scaled

# Problem parameters
numom = 2
mesh_names = ["mesh_om"]
num_problems = 4
criticalspeed = 1.0
max_num_problems = 5000

generate_meshes()

mumss2D.init_outfiles(numom)
mumss2D.load_background_mesh("mesh_bm")
xd = mumss2D.xbm_max - mumss2D.xbm_min
yd = mumss2D.ybm_max - mumss2D.ybm_min

# Define house position(s), [x, y, rotation angle]
def random_overlapping_positions(numom):
    overlapping_mesh_positions = []
    for m in range(numom):
        corr = 0.1
        xmin = 0.1*xd; xmax = 0.9*xd
        ymin = 0.1*yd; ymax = 0.9*yd
        xdiff = xmax - xmin; ydiff = ymax - ymin;
        xrestmin = m * xdiff / numom + corr;
        xrestmax = (m + 1 - numom) * xdiff / numom - corr
        yrestmin = 0.0; yrestmax = 0.0
        xmin += xrestmin; xmax += xrestmax
        ymin += yrestmin; ymax += yrestmax
        overlapping_mesh_positions.append([rm.uniform(xmin, xmax), \
                                rm.uniform(ymin, ymax), \
                                rm.uniform(0, 360)])

    return overlapping_mesh_positions

def run_fixed_number_of_problems(numom, mesh_names, num_problems):
    for i in range(num_problems):
        print("Stokes problem ID = ", i)
        mumss2D.run_mumss2D(mesh_names, random_overlapping_positions(numom))

def run_until_criticalspeed_reached(numom, mesh_names, criticalspeed, max_num_problems):
    runit = True
    i = 0
    while runit and i < max_num_problems:
        print("Stokes problem ID = ", i)
        om_stokes = random_overlapping_positions(numom)
        runit = mumss2D.debug_mumss2D(mesh_names, om_stokes, criticalspeed)
        i += 1

    # Solve a Poisson problem with the same multimesh setup for comparison
    run_poisson_problem(numom, overlapping_mesh_positions = om_stokes)

def run_poisson_problem(numom, overlapping_mesh_positions =
                        random_overlapping_positions(numom)):
    mumps2D.init_outfiles(numom)
    mumps2D.load_background_mesh()
    mumps2D.run_mumps2D(overlapping_mesh_positions)

run_main()
