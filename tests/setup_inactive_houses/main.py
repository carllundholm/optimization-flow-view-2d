import random as rm
import sys
from multimesh_stokes_2D import mumss2D
from multimesh_poisson_2D import mumps2D

# run_main() is called at the end.
# Choose termination criteria for running many problems
def run_main():
    run_fixed_number_of_problems(numh, num_problems)
    #run_until_criticalspeed_reached(numh, criticalspeed, max_num_problems)
    #run_poisson_problem(numh)

# Problem parameters
numh = 1
num_problems = 1
criticalspeed = 2.0
max_num_problems = 2000

mumss2D.init_outfiles(numh)
mumss2D.load_air_mesh()
xd = mumss2D.x_amax - mumss2D.x_amin
yd = mumss2D.y_amax - mumss2D.y_amin

# Define house position(s), [x, y, rotation angle]
def random_house_positions(numh):
    house_positions = []
    for h in range(numh):
        xmin = 0.4*xd; xmax = 0.6*xd
        ymin = 0.4*yd; ymax = 0.6*yd
        xdiff = xmax - xmin; ydiff = ymax - ymin;
        xrestmin = h * xdiff / numh; xrestmax = (h + 1 - numh) * xdiff / numh
        yrestmin = 0.0; yrestmax = 0.0
        xmin += xrestmin; xmax += xrestmax
        ymin += yrestmin; ymax += yrestmax
        house_positions.append([rm.uniform(xmin, xmax), \
                                rm.uniform(ymin, ymax), \
                                rm.uniform(0, 360)])

    #print(h, xmin, xmax)
    return house_positions

def run_fixed_number_of_problems(numh, num_problems):
    for i in range(num_problems):
        print("Stokes problem ID = ", i)
        mumss2D.run_mumss2D(random_house_positions(numh))

def run_until_criticalspeed_reached(numh, criticalspeed, max_num_problems):
    runit = True
    i = 0
    while runit and i < max_num_problems:
        print("Stokes problem ID = ", i)
        random_houses_stokes = random_house_positions(numh)
        runit = mumss2D.debug_mumss2D(random_houses_stokes, criticalspeed)
        i += 1

    # Solve a Poisson problem with the same multimesh setup for comparison
    run_poisson_problem(numh, house_positions = random_houses_stokes)

def run_poisson_problem(numh, house_positions = random_house_positions(numh)):
    mumps2D.init_outfiles(numh)
    mumps2D.load_air_mesh()
    mumps2D.run_mumps2D(house_positions)

run_main()
