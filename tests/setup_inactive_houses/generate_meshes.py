from fenics import *
from mshr import *

# Generate and save background mesh
mesh_bm = RectangleMesh(Point(0.0, 0.0), Point(1.0, 1.0), 12, 12)
File("meshes/mesh_air.xml") << mesh_bm
File("meshes_pvd/mesh_air.pvd") << mesh_bm

# Generate house box mesh
# Create house domain
l_hx = 0.2; l_hy = 0.5 * l_hx
x_hmin = -0.5*l_hx; y_hmin = -0.5*l_hy;
x_hmax = 0.5*l_hx; y_hmax = 0.5*l_hy
pi_fac = l_hx*2e-2*pi
p_h0 = Point(x_hmin, y_hmin) - pi_fac*Point(1.0, 1.0)
p_h1 = Point(x_hmax, y_hmax) + pi_fac*Point(1.0, 1.0)
domain_house = Rectangle(p_h0, p_h1)

# Create housebox domain
lb = 10*pi_fac
r = 0.5*sqrt(l_hx**2 + l_hy**2)
R = 2*r
domain_housebox = Rectangle(p_h0 - Point(lb, lb), p_h1 + Point(lb, lb))

# Generate and save housebox mesh
domain_housebox.set_subdomain(1, domain_house)
mesh_hb_init = generate_mesh(domain_housebox, 10)
File("meshes/mesh_hb_init.xml") << mesh_hb_init
File("meshes_pvd/mesh_hb_init.pvd") << mesh_hb_init

# Define house boundary
def on_house_boundary(x):
    if (near(x[0], p_h0[0]) and p_h0[1] <= x[1] and x[1] <= p_h1[1])  or \
       (near(x[0], p_h1[0]) and p_h0[1] <= x[1] and x[1] <= p_h1[1])  or \
       (near(x[1], p_h0[1]) and p_h0[0] <= x[0] and x[0] <= p_h1[0])  or \
       (near(x[1], p_h1[1]) and p_h0[0] <= x[0] and x[0] <= p_h1[0]):
        return True
    else:
        return False

# Mark the house boundary and interior in housebox mesh
house_boundary_markers = MeshFunction("size_t", mesh_hb_init, 1)
house_boundary_markers.set_all(0)
house_markers = MeshFunction("size_t", mesh_hb_init, 1)
house_markers.set_all(0)
for f in facets(mesh_hb_init):
    if domain_house.inside(f.midpoint()):
        if on_house_boundary(f.midpoint()):
            house_boundary_markers.set_value(f.index(), 1)
        else:
            vindex = f.entities(0)
            v0 = mesh_hb_init.coordinates()[vindex[0]]
            v1 = mesh_hb_init.coordinates()[vindex[1]]
            if not on_house_boundary(v0) and not on_house_boundary(v1):
                house_markers.set_value(f.index(), 1)

File("meshes/house_boundary_markers.xml") << house_boundary_markers
File("meshes_pvd/house_boundary_markers.pvd") << house_boundary_markers
File("meshes/house_markers.xml") << house_markers
File("meshes_pvd/house_markers.pvd") << house_markers

house_cell_markers = MeshFunction("size_t", mesh_hb_init, 2)
house_cell_markers.set_all(0)
for c in cells(mesh_hb_init):
    if domain_house.inside(c.midpoint()):
        house_cell_markers.set_value(c.index(), 1)
File("meshes/house_cell_markers.xml") << house_cell_markers
File("meshes_pvd/house_cell_markers.pvd") << house_cell_markers
