import random as rm
import sys
from multimesh_stokes_2D import mumss2D
from view_2D import view2D
from polygon_comp_2D import polygon_comp2D
import scipy.optimize
import numpy as np
from dolfin import *

# run_main() is called at the end.
# Choose termination criteria for running many problems
def run_main():
    run_fixed_number_of_problems(numh, num_problems)
    #run_optimization(numh, p_norm)


# Problem parameters
numh = 7
p_norm = 100
num_problems = 1

mumss2D.init_outfiles(numh)
mumss2D.load_air_mesh()
x_amin = mumss2D.x_amin; x_amax = mumss2D.x_amax
y_amin = mumss2D.y_amin; y_amax = mumss2D.y_amax
xd = x_amax - x_amin
yd = y_amax - y_amin

# Define house position(s), [x, y, rotation angle]
def random_house_positions(numh):
    house_positions = []
    for h in range(numh):
        extra_dist = 0.05*xd
        xmin = 0.1*xd; xmax = 0.9*xd
        ymin = 0.1*yd; ymax = 0.9*yd
        xdiff = xmax - xmin; ydiff = ymax - ymin;
        xrestmin = h * xdiff / numh + extra_dist
        xrestmax = (h + 1 - numh) * xdiff / numh - extra_dist
        yrestmin = 0.0; yrestmax = 0.0
        xmin += xrestmin; xmax += xrestmax
        ymin += yrestmin; ymax += yrestmax
        house_positions.append([rm.uniform(xmin, xmax), \
                                rm.uniform(ymin, ymax), \
                                rm.uniform(0, 360)])

    return house_positions

def random_house_positions_optimization(numh):
    house_positions = []
    for h in range(numh):
        extra_dist = 0.05*xd
        xmin = 0.1*xd; xmax = 0.9*xd
        ymin = 0.1*yd; ymax = 0.9*yd
        xdiff = xmax - xmin; ydiff = ymax - ymin;
        xrestmin = h * xdiff / numh + extra_dist
        xrestmax = (h + 1 - numh) * xdiff / numh - extra_dist
        yrestmin = 0.0; yrestmax = 0.0
        xmin += xrestmin; xmax += xrestmax
        ymin += yrestmin; ymax += yrestmax
        house_positions.append(rm.uniform(xmin, xmax))
        house_positions.append(rm.uniform(ymin, ymax))
        house_positions.append(rm.uniform(0, 360))

    return house_positions


def seven_house_positions_opt_hexagon(R = 0.08549942522316488):
    house_positions = []
    radius = 2.0*R
    center = [0.56, 0.32]
    house_positions.append(center[0])
    house_positions.append(center[1])
    house_positions.append(0.0)
    for h in range(6):
        angle = h*pi/3
        x = center[0] + radius*cos(angle)
        y = center[1] + radius*sin(angle)
        house_positions.append(x)
        house_positions.append(y)
        house_positions.append(0.0)

    return house_positions

def seven_house_positions_opt_horizontal_line(dist = 0.117465586529):
    house_positions = []
    hp0 = [0.128, 0.42, 90.0]
    house_positions.append(hp0[0])
    house_positions.append(hp0[1])
    house_positions.append(hp0[2])
    for h in range(1, 7):
        house_positions.append(hp0[0] + h*dist)
        house_positions.append(hp0[1])
        house_positions.append(hp0[2])

    return house_positions

def seven_house_positions_opt_vertical_line(dist = 0.0709153870112):
    house_positions = []
    hp0 = [0.62, 0.08, 0.0]
    house_positions.append(hp0[0])
    house_positions.append(hp0[1])
    house_positions.append(hp0[2])
    for h in range(1, 7):
        house_positions.append(hp0[0])
        house_positions.append(hp0[1] + h*dist)
        house_positions.append(hp0[2])

    return house_positions

def seven_house_positions_opt_principal_line():
    p0 = np.array([0.17, 0.51])
    p1 = np.array([0.76, 0.24])
    v = p1 - p0
    angle = 90.0 - (180.0/pi)*atan(abs(v[1]/v[0]))
    house_positions = []
    house_positions.append(p0[0])
    house_positions.append(p0[1])
    house_positions.append(angle)
    for h in range(1, 7):
        house_positions.append(p0[0] + h/6.0*v[0])
        house_positions.append(p0[1] + h/6.0*v[1])
        house_positions.append(angle)

    return house_positions

def seven_house_positions_opt_close2sea():
    house_positions = []

    # House 0
    house_positions.append(0.25)
    house_positions.append(0.55)
    house_positions.append(6.0)

    # House 1
    house_positions.append(0.51)
    house_positions.append(0.53)
    house_positions.append(-17.0)

    # House 2
    house_positions.append(0.75)
    house_positions.append(0.45)
    house_positions.append(-25)

    # House 3
    house_positions.append(0.78)
    house_positions.append(0.24)
    house_positions.append(-115.0)

    # House 4
    house_positions.append(0.62)
    house_positions.append(0.1)
    house_positions.append(0.0)

    # House 5
    house_positions.append(0.39)
    house_positions.append(0.21)
    house_positions.append(-30.0)

    # House 6
    house_positions.append(0.17)
    house_positions.append(0.39)
    house_positions.append(-41.0)

    return house_positions

def run_fixed_number_of_problems(numh, num_problems):
    for i in range(num_problems):
        print("Stokes problem ID = ", i)
        #hps = random_house_positions(numh)
        #hps = seven_house_positions_opt_hexagon()
        #hps = seven_house_positions_opt_horizontal_line()
        #hps = seven_house_positions_opt_vertical_line()
        #hps = seven_house_positions_opt_principal_line()
        hps = seven_house_positions_opt_close2sea()
        #mumss2D.run_mumss2D(hps)

        mumss2D.velocity_for_optimization(hps)
        mumss2D.place_and_save_house_meshes(hps)
        mumss2D.save_solution()


def run_optimization(numh):
    # Compute the max radius r of a house
    house_dims = np.load("meshes/house_dimensions.npy")
    r = 0.5*sqrt(house_dims[0]**2 + house_dims[1]**2)
    #r *= 1.0 + 1e-3

    # Compute the max radius R of a house box mesh
    mesh_hb = Mesh("meshes/mesh_hb_init.xml")
    xs_hb = mesh_hb.coordinates()
    xmin = min([x[0] for x in xs_hb])
    xmax = max([x[0] for x in xs_hb])
    ymin = min([x[1] for x in xs_hb])
    ymax = max([x[1] for x in xs_hb])
    R = 0.5*sqrt((xmax - xmin)**2 + (ymax - ymin)**2)
    #R *= 1.0 + 1e-3

    # Initialize island polygon for "stay on island" constraints
    polygon = np.load("meshes/island_boundary_cc.npy")
    polygon_comp2D(polygon)

    # Compute feasible random initial positions for houses on island
    min_dist = -1e10
    while min_dist <= 0.0:
        hps0 = np.array(random_house_positions_optimization(numh))
        min_dist = 1e10
        for i in range(numh):
            minsbdi = polygon_comp2D.compute_min_sbd(np.array([hps0[3*i], hps0[3*i + 1]])) - r**2
            min_dist = min(min_dist, minsbdi)

    # Define "staying in background mesh" bounds
    bnds = ()
    for i in range(numh):
        bnds += ((x_amin + R, x_amax - R),
                 (y_amin + R, y_amax - R),
                 (None, None))

    # Define "keep distance between houses" constraints
    cons = ()
    for i in range(numh - 1):
        for j in range(i+1, numh):
            cons += ({"type": "ineq",
                      "fun" : lambda x: (x[3*i] - x[3*j])**2 \
                                      + (x[3*i + 1] - x[3*j + 1])**2 \
                                      - (2.0*R)**2},)

    # Define "keep distance between houses" constraints (USED IN PENALTY FUNCTION)
    def keep_distance_between_houses_penalty(dx, dy):
        d_sqrd = dx**2 + dy**2
        g = d_sqrd - (2.0*R)**2

        ReQU = min(0, g)**2

        return ReQU

    # Define "stay on island" constraints (USED IN PENALTY FUNCTION)
    def stay_on_island_penalty(x, y):
        d_sqrd = polygon_comp2D.compute_min_sbd(np.array([x, y]))
        g = d_sqrd - r**2

        ReQU = min(0, g)**2

        return ReQU

    # Define penalty function
    def penalty_function(hps0):
        value_kdbh = 0.0; value_soi = 0.0
        pnlt_param_kdbh = 1e3; pnlt_param_soi = 1e8

        # "Keep distance between houses" contribution
        for i in range(numh - 1):
            for j in range(i + 1, numh):
                dx = hps0[3*i] - hps0[3*j]
                dy = hps0[3*i + 1] - hps0[3*j + 1]
                value_kdbh += keep_distance_between_houses_penalty(dx, dy)

        # "Stay on island" contribution
        for i in range(numh):
            x = hps0[3*i]
            y = hps0[3*i + 1]
            value_soi += stay_on_island_penalty(x, y)

        print(value_kdbh, value_soi)
        return pnlt_param_kdbh*value_kdbh + pnlt_param_soi*value_soi

    # Define objective function
    def objective_function(hps0):
        #value = mumss2D.velocity_for_optimization(hps0)
        value = view2D.view_for_optimization(hps0)
        value += penalty_function(hps0)
        return value

    # Start optimization
    #hps1 = 0; objective_function(hps0)

    # Methods with NO gradient evaluations
    #hps1 = scipy.optimize.minimize(objective_function, hps0, method = 'Nelder-Mead', options={'maxiter':9000, 'maxfev':9000})
        # Old method with well-known weaknesses
    hps1 = scipy.optimize.minimize(objective_function, hps0, method = 'Powell', options={'maxfev':10000})
        # Seems to be most suitable, take more fev but gives better solutions for minVh5, maxWh3
    #hps1 = scipy.optimize.minimize(objective_function, hps0, method = 'COBYLA', options={'maxiter':1000})
        # Gives many suboptimal solutions, minVh5 <= 0.89

    # Methods with gradient evaluations (approximated) and NO bounds
    #hps1 = scipy.optimize.minimize(objective_function, hps0, method = 'BFGS')
        # Does not converge for minVh5, needs hessian as well (msg:'Desired error not necessarily achieved due to precision loss.')

    # Methods with gradient evaluations (approximated) and bounds
    #hps1 = scipy.optimize.minimize(objective_function, hps0, method = 'TNC', bounds = bnds, options={'maxiter':10000})
        # Gives many suboptimal solutions, minVh5 <= 0.89
    #hps1 = scipy.optimize.minimize(objective_function, hps0, method = 'L-BFGS-B', bounds = bnds)
        # Gives many suboptimal solutions, minVh5 <= 0.90
    #hps1 = scipy.optimize.minimize(objective_function, hps0, method = 'SLSQP', bounds = bnds)
        # Gives many suboptimal solutions, minVh5 <= 0.90 and performs even worse without supplied bounds

    # Check distance between houses is fulfilled
    for i in range(numh - 1):
        for j in range(i+1, numh):
            dx = hps1.x[3*i] - hps1.x[3*j]
            dy = hps1.x[3*i + 1] - hps1.x[3*j + 1]
            cons_ij = dx**2 + dy**2 - (2.0*R)**2
            print("Extra distance between houses %d and %d is %f" %(i, j, cons_ij))
            print("Penalty =", keep_distance_between_houses_penalty(dx, dy))


    # Check that the houses lie on the island
    for i in range(numh):
        x = hps1.x[3*i]
        y = hps1.x[3*i + 1]
        minsbdi = polygon_comp2D.compute_min_sbd(np.array([x, y])) - r**2
        print("House %d squared boundary distance = %f" %(i, minsbdi))
        print("Penalty =", stay_on_island_penalty(x, y))

    print("Penalty function value for solution =", penalty_function(hps1.x))

    # Save meshes, Stokes solution using the positions from optimization solution
    mumss2D.place_and_save_house_meshes(hps1.x)
    #mumss2D.save_solution()

    return hps1

def run_test():
    house_dims = np.load("meshes/house_dimensions.npy")

    mesh_hb = Mesh("meshes/mesh_hb_init.xml")
    xs_hb = mesh_hb.coordinates()
    xmin = min([x[0] for x in xs_hb])
    xmax = max([x[0] for x in xs_hb])
    ymin = min([x[1] for x in xs_hb])
    ymax = max([x[1] for x in xs_hb])
    R = 0.5*sqrt((xmax - xmin)**2 + (ymax - ymin)**2)
    dist_horizontal = 1.09*(ymax - ymin)
    dist_vertical = 1.02*0.5*((ymax - ymin) + house_dims[1])
    #print(R, dist_horizontal, dist_vertical)
    #hps0 = seven_house_positions_opt_hexagon(R)
    #hps0 = seven_house_positions_opt_horizontal_line(dist_horizontal)
    #hps0 = seven_house_positions_opt_vertical_line(dist_vertical)
    #hps0 = seven_house_positions_opt_principal_line()
    hps0 = seven_house_positions_opt_close2sea()
    mumss2D.place_and_save_house_meshes(hps0)


run_main()
#run_test()
#hps1 = run_optimization(numh)
#print(hps1)
#print("The minimum view from a house is", 1.0 - hps1.fun)
