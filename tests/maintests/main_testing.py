import random as rm
import numpy as np
import sys
from multimesh_stokes_2D import mumss2D
from view_2D import view2D
from in_polygon_2D import in_polygon2D
from multimesh_poisson_2D import mumps2D

# run_main() is called at the end.
# Choose termination criteria for running many problems
def run_main():
    #run_fixed_number_of_problems(numh, num_problems)
    #run_until_criticalspeed_reached(numh, criticalspeed, max_num_problems)
    #run_poisson_problem(numh)
    #run_view_random_tests(numh, num_problems)
    #run_view_specific_test()
    #run_in_polygon_test1()
    run_in_polygon_test2()

# Problem parameters
numh = 200
num_problems = 10
criticalspeed = 2.0
max_num_problems = 2500

mumss2D.init_outfiles(numh)
mumss2D.load_air_mesh()
xd = mumss2D.x_amax - mumss2D.x_amin
yd = mumss2D.y_amax - mumss2D.y_amin

# Define house position(s), [x, y, rotation angle]
def random_house_positions(numh):
    house_positions = []
    for h in range(numh):
        extra_dist = 0.1*xd
        xmin = 0.1*xd; xmax = 0.9*xd
        ymin = 0.1*yd; ymax = 0.9*yd
        xdiff = xmax - xmin; ydiff = ymax - ymin;
        xrestmin = h * xdiff / numh + extra_dist
        xrestmax = (h + 1 - numh) * xdiff / numh - extra_dist
        yrestmin = 0.0; yrestmax = 0.0
        xmin += xrestmin; xmax += xrestmax
        ymin += yrestmin; ymax += yrestmax
        house_positions.append([rm.uniform(xmin, xmax), \
                                rm.uniform(ymin, ymax), \
                                rm.uniform(0, 360)])

    #print(h, xmin, xmax)
    return house_positions

def run_fixed_number_of_problems(numh, num_problems):
    for i in range(num_problems):
        print("Stokes problem ID = ", i)
        mumss2D.run_mumss2D(random_house_positions(numh))

def run_until_criticalspeed_reached(numh, criticalspeed, max_num_problems):
    runit = True
    i = 0
    while runit and i < max_num_problems:
        print("Stokes problem ID = ", i)
        random_houses_stokes = random_house_positions(numh)
        runit = mumss2D.debug_mumss2D(random_houses_stokes, criticalspeed)
        i += 1

    if not runit:
        # Solve a Poisson problem with the same multimesh setup for comparison
        run_poisson_problem(numh, house_positions = random_houses_stokes)

def run_poisson_problem(numh, house_positions = random_house_positions(numh)):
    mumps2D.init_outfiles(numh)
    mumps2D.load_air_mesh()
    mumps2D.run_mumps2D(house_positions)

def run_view_random_tests(numh, num_problems):
    for i in range(num_problems):
        print("View computation ID = ", i)
        View = view2D.compute_total_view(numh, random_house_positions(numh))
        print("The view is %f" %View)

def run_view_specific_test():
    house_dims = np.load("meshes/house_dimensions.npy")
    lx = house_dims[0]; ly = house_dims[1]
    hp0 = [0.0, 0.0, 0.0]
    a = 0.5*(lx + ly)/np.sqrt(2.0)
    hp1 = [-a, a, 45.0]
    hp2 = [-0.5*a, 0.5*a, 45.0]
    hp3 = [-2*a, 0.5*a, 45.0]
    #hp1 = [-0.5*ly, 0.0, 90.0]
    hps = [hp0, hp1]
    #hps = [hp0, hp1, hp2, hp3]
    View = view2D.compute_total_view(len(hps), hps)
    print("The view is %f" %View)


def run_in_polygon_test1():
    #polygon = np.load("meshes/island_boundary_cc.npy")
    polygon = np.array([np.array([0.0, 0.0]),
                        np.array([0.5, 0.2]),
                        np.array([1.0, 0.0]),
                        np.array([1.0, 1.0]),
                        np.array([0.0, 1.0])])
    in_polygon2D(polygon)
    numh = 1
    #house_positions = random_house_positions(numh)
    house_positions = [0.5, 0.5, 0.0]
    for i in range(numh):
        xi = house_positions[3*i]
        yi = house_positions[3*i + 1]
        minsbdi = in_polygon2D.compute_min_sbd(np.array([xi, yi]))
        print("House %d" %i)
        print("Position = [%f, %f], min_sbd = %f" %(xi, yi, minsbdi))
        print("Position = [%f, %f], min_dist = %f" %(xi, yi, np.sqrt(minsbdi)))

def run_in_polygon_test2():
    polygon = np.load("meshes/island_boundary_cc.npy")
    print(polygon)
    in_polygon2D(polygon)
    sbd = in_polygon2D.compute_min_sbd(np.array([8.63556157e-01, 4.92677677e-01]))
    print(sbd)


run_main()
