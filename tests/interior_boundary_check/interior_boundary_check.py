from dolfin import *
from mshr import *

# Check to see what happens with the interior boundary of an overlapping mesh
# when using auto_cover.

# Generate background mesh
d0 = Rectangle(Point(0.0, 0.0), Point(1.0, 1.0))
mesh0 = generate_mesh(d0, 10)

# Generate overlapping mesh with hole
hole = Rectangle(Point(0.4, 0.4), Point(0.6, 0.6))
mp = Point(0.5, 0.5)
lh = (2.0 + (pi - 3.0))*mesh0.hmax()
d1p0 = hole.first_corner() - Point(lh, lh)
d1p1 = hole.second_corner() + Point(lh, lh)
d1 = Rectangle(d1p0, d1p1)
mesh1 = generate_mesh(d1 - hole, 15)

# Save meshes
File("results/mesh0.pvd") << mesh0
File("results/mesh1.pvd") << mesh1

# Construct multimesh hierarchy
mm = MultiMesh()
mm.add(mesh0)
mm.add(mesh1)
mm.build()

# Compare interface lenghts
exterior_length = 2.0*(d1.second_corner().x() - d1.first_corner().x()) + \
                  2.0*(d1.second_corner().y() - d1.first_corner().y())
interior_length = 2.0*(hole.second_corner().x() - hole.first_corner().x()) + \
                  2.0*(hole.second_corner().y() - hole.first_corner().y())
total_length = exterior_length + interior_length
mm_interface_length_initial = mm.compute_area()

# Call to auto_cover
mm.auto_cover(0, Point(0.5, 0.5))

# Recompute inteface length
mm_interface_length_post_auto_cover = mm.compute_area()

# Print results
print("exterior_length =", exterior_length)
print("interior_length =", interior_length)
print("total_length =", total_length)
print("mm_interface_length_initial =", mm_interface_length_initial)
print("mm_interface_length_post_auto_cover =", mm_interface_length_post_auto_cover)
