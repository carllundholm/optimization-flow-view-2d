from fenics import *
from mshr import *
import numpy as np
import sys

### Generate meshes ###
# Generate a mesh with a hole
x_amin = 0.0; x_amax = 1.0
y_amin = 0.0; y_amax = 1.0
domain = Rectangle(Point(x_amin, y_amin), Point(x_amax, y_amax))
x_hmin = 0.25; x_hmax = 0.75
y_hmin = 0.40; y_hmax = 0.60
hole = Rectangle(Point(x_hmin, y_hmin), Point(x_hmax, y_hmax))
mesh_air = generate_mesh(domain - hole, 45)

# Mark hole boundary
hole_boundary_markers = MeshFunction("size_t", mesh_air, 1)
hole_boundary_markers.set_all(0)
for f in facets(mesh_air):
    if hole.inside(f.midpoint()):
        hole_boundary_markers.set_value(f.index(), 1)

File("results_1hole-mesh/hole_boundary_markers.xml") << hole_boundary_markers
File("results_1hole-mesh/hole_boundary_markers.pvd") << hole_boundary_markers

# Generate house meshes
numh = 0



mac = mesh_air.coordinates()
mac_x = np.array([]); mac_y = np.array([])
for coord in mac:
    mac_x = np.append(mac_x, coord[0])
    mac_y = np.append(mac_y, coord[1])

x_amin = min(mac_x); y_amin = min(mac_y)
x_amax = max(mac_x); y_amax = max(mac_y)

p_a0 = Point(x_amin, y_amin)
p_a1 = Point(x_amax, y_amax)

# Initialize multimesh and add air mesh
multimesh = MultiMesh()
multimesh.add(mesh_air)

# Build multimesh hierarchy
multimesh.build()

### Define additional problem data ###
# Define source term
f = Constant((0.0, 0.0))

# Define boundary regions
class XminBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], x_amin)

class XmaxBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], x_amax)

class YBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (near(x[0], x_amin) or
                                near(x[0], x_amax))

class YminBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], y_amin)

class YmaxBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], y_amax)

class XBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (near(x[1], y_amin) or
                                near(x[1], y_amax))


### Define and solve FE-variational formulation ###
# Create function space
P2 = VectorElement("P", triangle, 2)
P1 = FiniteElement("P", triangle, 1)
TH = P2 * P1
W = MultiMeshFunctionSpace(multimesh, TH)

# Define trial and test functions
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)

# Define normal and mesh size
n = FacetNormal(multimesh)
h = 2.0 * Circumradius(multimesh)

# Parameters and functions for bilinear form
alpha = Constant(1e1)
beta = Constant(1e0)
gamma = Constant(0e0)

def tensor_jump(v, n):
    return outer(v('+'), n('+')) + outer(v('-'), n('-'))

def a_h(v, w):
    return inner(grad(v), grad(w)) * dX \
         - inner(avg(grad(v)), tensor_jump(w, n)) * dI \
         - inner(avg(grad(w)), tensor_jump(v, n)) * dI \
         + alpha/avg(h) * inner(jump(v), jump(w)) * dI

def b_h(v, q):
    return -div(v) * q * dX + jump(v, n) * avg(q) * dI

#def s_O(v, w):
#    return beta * inner(jump(grad(v)), jump(grad(w))) * dO

def s_O(v, w):
    return (beta / avg(h)**2) * inner(jump(v), jump(w)) * dO

def s_P(q, r):
    return gamma * inner(jump(q), jump(r)) * dO

def s_C(v, q, w, r):
    return h*h * inner(div(grad(v)) - grad(q), div(grad(w)) + grad(r)) * dC

# Functions for linear form
def l_h(f, v):
    return inner(f, v) * dX

def l_C(f, v, q):
    return -h*h * inner(f, div(grad(v)) + grad(q)) * dC

# Define bilinear form
a = a_h(u, v) + b_h(u, q) + b_h(v, p) + s_O(u, v) + s_C(u, p, v, q)
#a = a_h(u, v) + b_h(u, q) + b_h(v, p) + s_O(u, v) + s_C(u, p, v, q) + s_P(p, q)

# Define linear form
L = l_h(f, v) + l_C(f, v, q)

# Assemble linear system
A = assemble_multimesh(a)
b = assemble_multimesh(L)

# Define boundary values
inflow_value = Constant((1.0/sqrt(2.0), 1.0/sqrt(2.0)))
outflow_value = Constant(0.0)
noslip_value = Constant((0.0, 0.0))

# Create subdomains for boundary conditions
xmin_boundary = XminBoundary()
xmax_boundary = XmaxBoundary()
ymin_boundary = YminBoundary()
ymax_boundary = YmaxBoundary()

# Create subspaces for boundary conditions
V = MultiMeshSubSpace(W, 0)
Q = MultiMeshSubSpace(W, 1)

# Create boundary conditions
bc_xmin = MultiMeshDirichletBC(V, inflow_value, xmin_boundary)
bc_xmax = MultiMeshDirichletBC(Q, outflow_value, xmax_boundary)
bc_ymin = MultiMeshDirichletBC(V, inflow_value, ymin_boundary)
bc_ymax = MultiMeshDirichletBC(Q, outflow_value, ymax_boundary)

# Apply boundary conditions
bc_xmin.apply(A, b)
bc_xmax.apply(A, b)
bc_ymin.apply(A, b)
bc_ymax.apply(A, b)

# Set noslip on the hole boundary
hole_noslip = MultiMeshDirichletBC(V, noslip_value, hole_boundary_markers, 1, 0)
hole_noslip.apply(A, b)

# Set noslip on the house boundary(s)
if numh > 0:
    for hb in range(numh):
        ff = MeshFunction("size_t", mesh_housebox[hb], "results_1hole-mesh/hole_boundary_markers.xml")
        File("results_1hole-mesh/hole_boundary_markers"+str(hb)+".pvd") << ff
        house_noslip = MultiMeshDirichletBC(V, noslip_value, ff, 1, hb + 1)
        house_noslip.apply(A, b)

# Set inactive dofs to zero
W.lock_inactive_dofs(A, b)

# Compute solution
w = MultiMeshFunction(W)
solve(A, w.vector(), b)

# Set noslip on the house(s) for post-solving visualisation purposes
if numh > 0:
    for hb in range(numh):
        ff = MeshFunction("size_t", mesh_housebox[hb], "results_1hole-mesh/interior_markers.xml")
        File("results_1hole-mesh/house_markers"+str(hb)+".pvd") << ff
        house_noslip = MultiMeshDirichletBC(V, noslip_value, ff, 1, hb + 1)
        house_noslip.apply(w.vector())

# Save solution to file
u_air = w.part(0).sub(0)
p_air = w.part(0).sub(1)
File("results_1hole-mesh/u_air.pvd") << u_air
File("results_1hole-mesh/p_air.pvd") << p_air

if numh > 0:
    for hb in range(numh):
        u_hb = w.part(hb + 1).sub(0)
        p_hb = w.part(hb + 1).sub(1)

        File("results_1hole-mesh/u_hb"+str(hb)+".pvd") << u_hb
        File("results_1hole-mesh/p_hb"+str(hb)+".pvd") << p_hb
