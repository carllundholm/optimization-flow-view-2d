from dolfin import *
import numpy as np

def find_cell(multimesh, point):
    """ Find cell that contains given point """

    for cell_id in list(multimesh.cut_cells(0)) + list(multimesh.uncut_cells(0)):
        cell = Cell(multimesh.part(0), cell_id)
        if cell.contains(point):
            return cell
    plot(multimesh.part(0))
    plot(multimesh.part(1), color="r")
    import matplotlib.pyplot as plt
    plt.show()
    raise(ValueError, "No cut or uncut cell found with given point")

def cell_neighbours(mesh):
    """ Returns a mapping from cell to its cell neighbours """
    # Init facet-cell connectivity
    tdim = mesh.topology().dim()
    mesh.init(tdim - 1, tdim)

    # For every cell, build a list of cells that are connected to its facets
    # but are not the iterated cell
    cell_neighbours = {cell.index(): sum([list(filter(lambda ci: ci != cell.index(),
                                                     facet.entities(tdim)))
                                         for facet in facets(cell)], [])
                       for cell in cells(mesh)}

    return cell_neighbours

def cover_hole(multimesh, point, visualization=False, figprefix="covered_cells"):
    """ Marks all cells connected to the given point as covered.
        This can be used for instance to mark a hole as covered
        where one point inside the hole is known.
    """
    covered_cells = multimesh.covered_cells(0)
    cut_cells = multimesh.cut_cells(0)
    new_covered_cells = []
    neighbours = cell_neighbours(multimesh.part(0))

    def cover_recursive(cell):

        if cell.index() in covered_cells:
            return

        new_covered_cells.append(cell.index())

        for neighbour_id in neighbours[cell.index()]:
            if neighbour_id not in new_covered_cells:
                cover_recursive(Cell(multimesh.part(0), neighbour_id))

    start_cell = find_cell(multimesh, point)
    cell_markers = MeshFunction("size_t", multimesh.part(0), 2)

    cover_recursive(start_cell)
    multimesh.mark_covered(0, np.array(new_covered_cells, dtype="uintc"))

    if visualization:
        cell_markers = MeshFunction("size_t", multimesh.part(0), 2)
        for cell in new_covered_cells:
            cell_markers[cell] = 1
        import matplotlib.pyplot as plt
        plot(cell_markers)
        plot(multimesh.part(0), color="w", linewidth=0.8)
        for i in range(multimesh.num_parts()-1):
            plot(multimesh.part(i+1))
        #plt.show()
        plt.savefig(fixprefix + ".png")

if __name__=="__main__":
    # Get mesh and meshfunction from file
    mesh_0 = Mesh("meshes/multimesh_background_symmetric.xml")
    mesh_1 = Mesh("meshes/multimesh_front_symmetric.xml")
    mesh_1.rotate(3)
    multimesh = MultiMesh()
    multimesh.add(mesh_0)
    multimesh.add(mesh_1)
    multimesh.build()

    # Set inactive dofs
    center = [0.5,0.5]
    center = Point(center[0], center[1])
    print( len(multimesh.covered_cells(0)))
    cover_hole(multimesh,center,visualization=True)
    print( len(multimesh.covered_cells(0)))
