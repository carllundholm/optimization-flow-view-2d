from fenics import *

class mumps2D:

    # Create files for storing meshes and solutions
    @classmethod
    def init_outfiles(cls, numh):
        cls.outfile_u_air = File("solutions/u_poisson_air.pvd")
        cls.outfile_u_hb = []
        for hb in range(numh):
            cls.outfile_u_hb.append(File("solutions/u_poisson_hb"+str(hb)+".pvd"))

    # Load background air mesh
    @classmethod
    def load_air_mesh(cls):
        cls.mesh_air = Mesh("meshes/mesh_air.xml")

        # Get air mesh min and max coordinates
        xs = cls.mesh_air.coordinates()
        cls.x_amin = min([x[0] for x in xs])
        cls.x_amax = max([x[0] for x in xs])
        cls.y_amin = min([x[1] for x in xs])
        cls.y_amax = max([x[1] for x in xs])

    # Place house meshes and create multimesh hierarchy
    @classmethod
    def place_house_meshes(cls, house_positions):

        # Initialize multimesh and first add background air mesh
        cls.multimesh = MultiMesh()
        cls.multimesh.add(cls.mesh_air)

        # Place houses
        cls.numh = len(house_positions)
        if cls.numh > 0:
            cls.mesh_housebox = []
            for hb in range(cls.numh):
                mesh_hb = Mesh("meshes/mesh_hb_init.xml")

                # Rotate house
                angle = house_positions[hb][2]
                mesh_hb.rotate(angle, 2)

                # Translate house
                trans_point = Point(house_positions[hb][0], house_positions[hb][1])
                mesh_hb.translate(trans_point)

                # Add to multimesh hierarchy
                cls.mesh_housebox.append(mesh_hb)
                cls.multimesh.add(mesh_hb)

        # Build multimesh hierarchy
        cls.multimesh.build()

    # Save house meshes to file
    @classmethod
    def save_house_meshes(cls):
        for hb in range(cls.numh):
            cls.outfile_mesh_house[hb] << cls.mesh_housebox[hb]

    # Clear multimesh hierarchy
    @classmethod
    def clear_meshes(cls):
        cls.multimesh.clear()

    # Set the predefined problem data
    @classmethod
    def set_problem_data(cls):
        # Define source term
        f = Constant(1.0)

        # Define boundary regions
        class XminBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[0], cls.x_amin)

        class XmaxBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[0], cls.x_amax)

        class YBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (near(x[0], cls.x_amin) or
                                        near(x[0], cls.x_amax))

        class YminBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[1], cls.y_amin)

        class YmaxBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[1], cls.y_amax)

        class XBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (near(x[1], cls.y_amin) or
                                        near(x[1], cls.y_amax))

        class Boundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary

        ### Define FE-variational formulation ###
        # Create function space
        cls.V = MultiMeshFunctionSpace(cls.multimesh, "P", 1)

        # Define trial and test functions
        u = TrialFunction(cls.V)
        v = TestFunction(cls.V)

        # Define normal and mesh size
        n = FacetNormal(cls.multimesh)
        h = 2.0 * Circumradius(cls.multimesh)
        h = (h('+') + h('-')) / 2

        # Parameters and functions for bilinear form
        alpha = 10.0
        beta = 1.0

        # Define bilinear form
        a = dot(grad(u), grad(v))*dX \
          - dot(avg(grad(u)), jump(v, n))*dI \
          - dot(avg(grad(v)), jump(u, n))*dI \
          + alpha/h * jump(u)*jump(v)*dI \
          + beta/h**2 * dot(jump(u), jump(v))*dO

        # Define linear form
        L = f*v*dX

        # Assemble linear system
        cls.A = assemble_multimesh(a)
        cls.b = assemble_multimesh(L)

        # Define boundary values
        cls.homoD_value = Constant(0.0)

        # Create subdomains for boundary conditions
        boundary = Boundary()

        # Create boundary conditions
        bc = MultiMeshDirichletBC(cls.V, cls.homoD_value, boundary)

        # Apply boundary conditions
        bc.apply(cls.A, cls.b)

        # Set homogeneous Dirichlet on the house boundar(y/ies)
        for hb in range(cls.numh):
            ff = MeshFunction("size_t", cls.mesh_housebox[hb], "meshes/house_boundary_markers.xml")
            house_boundary_homoD = MultiMeshDirichletBC(cls.V, cls.homoD_value, ff, 1, hb + 1)
            house_boundary_homoD.apply(cls.A, cls.b)

        # Set inactive dofs to zero
        cls.V.lock_inactive_dofs(cls.A, cls.b)

    @classmethod
    def solve_problem(cls):
        # Compute solution
        cls.u = MultiMeshFunction(cls.V)
        solve(cls.A, cls.u.vector(), cls.b)

    @classmethod
    def save_solution(cls):
        # Save solution to file
        u_air = cls.u.part(0)
        cls.outfile_u_air << u_air

        for hb in range(cls.numh):
            u_hb = cls.u.part(hb + 1)
            cls.outfile_u_hb[hb] << u_hb

    @classmethod
    def run_mumps2D(cls, house_positions):
        # Set house meshes
        cls.place_house_meshes(house_positions)
        print("Background mesh set")
        print(cls.numh, "additional overlapping meshes set")

        # Save house meshes
        #cls.save_house_meshes()

        # Define problem
        print("Setting Poisson problem data ...")
        cls.set_problem_data()
        print("Poisson problem data set")

        # Solve problem
        print("Solving Poisson problem ...")
        cls.solve_problem()
        print("Poisson problem solved")

        # Save solution
        cls.save_solution()

        # Clear multimesh hierarchy
        cls.clear_meshes()
        print("multimesh hierarchy cleared")
