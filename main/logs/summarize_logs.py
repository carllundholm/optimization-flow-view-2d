# Python script for summarizing desired info from log files in cwd to file "summarized_logs.txt"
import os

# Find all log files in cwd and put them in a list
logfiles = [f for f in os.listdir() if "opt_" in f and ".log" in f]

# Open the write file
writefile = open('summarized_logs.txt', 'w')

# Loop over the log files
for logfile in logfiles:
    # Open the log file as read file
    with open (logfile, 'rt') as readfile:
        writefile.write("### " + readfile.name + " ###\n")

        # Find and write the first occurence of string in if-statement
        for line in readfile:
            if "The largest L100-norm of house velocities" in line:
                writefile.write("WIND \n")
                writefile.write("Initial: ")
                writefile.write(line)
                break

        # Find and write the last occurence of string in if-statement
        for line in reversed(list(readfile)):
            if "The largest L100-norm of house velocities" in line:
                writefile.write("Final: ")
                writefile.write(line)
                break

        # Reset line counter for reading readfile
        readfile.seek(0)

        # Find and write the first occurence of string in if-statement
        for line in readfile:
            if "Maximum hidden sector is" in line:
                writefile.write("VIEW \n")
                writefile.write("Initial: ")
                writefile.write(line)
                break

        # Find and write the last occurence of string in if-statement
        for line in reversed(list(readfile)):
            if "Maximum hidden sector is" in line:
                writefile.write("Final: ")
                writefile.write(line)
                break

        writefile.write("\n")

writefile.close()
