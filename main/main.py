# Script for calling run_optimization([weight_wind, weight_view],
#                                     [wind_in_x, wind_in_y],
#                                     config_init,
#                                     prefix = "recent",
#                                     numh = 7)
# Possible values for <config_init> are "hexagon", "principal_line", "coastal", "random"

from optimization import *
import sys

# Set default values
weight_wind = 1.0;
weight_view = 0.0;
wind_type = "diagonal"
config_init = "random"
prefix = "opt_wind_diagonal_random"

# Read supplied arguments if given
if len(sys.argv) == 5:
    weight_wind = float(sys.argv[1])
    weight_view = 1.0 - weight_wind
    wind_type = sys.argv[2]
    config_init = sys.argv[3]
    prefix = sys.argv[4]

if wind_type == "horizontal":
    wind_in = [1.0, 0.0]
elif wind_type == "vertical":
    wind_in = [0.0, 1.0]
else:
    wind_type = "diagonal"
    wind_in = [2.0/sqrt(5.0), 1.0/sqrt(5.0)]

print("From main.py:")
print("weight_wind =", weight_wind)
print("weight_view =", weight_view)
print("wind_type =", wind_type)
print("wind_in =", wind_in)
print("config_init =", config_init)
print("prefix =", prefix)

run_optimization([weight_wind, weight_view], wind_in, config_init, prefix)
