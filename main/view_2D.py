import numpy as np

class view2D:

    @classmethod
    def compute_Xaxis_angle(cls, p0, p1):
        # Compute angle between X-axis through p0 and segment between p0 & p1
        x = p1[0] - p0[0]; y = p1[1] - p0[1];
        if x > 0.0:
            alpha = np.arctan(y/x)
        elif x < 0.0:
            alpha = np.sign(np.sign(y) - 0.5)*np.pi + np.arctan(y/x)
        else: #(x = 0)
            alpha = 0.5*np.sign(y)*np.pi

        return alpha

    @classmethod
    def compute_corners_of_house(cls, k):
        # Position of house k
        xk = cls.house_positions[k][0]
        yk = cls.house_positions[k][1]
        thetak = cls.house_positions[k][2]*np.pi/180.0
        cos_tk = np.cos(thetak)
        sin_tk = np.sin(thetak)

        # Loop through the corners
        corners = []
        for c in range(4):
            aclx = (-1.0)**np.floor(1.5*c)*cls.lx
            bcly = (-1.0)**np.ceil(1.5*c)*cls.ly
            co_x = 0.5*(cos_tk*aclx - sin_tk*bcly) + xk
            co_y = 0.5*(sin_tk*aclx + cos_tk*bcly) + yk
            corners.append([co_x, co_y])

        return corners

    @classmethod
    def compute_view_from_house(cls, k):
        h0 = cls.house_positions[k]
        p0 = [h0[0], h0[1]]

        # Loop over all the other houses to compute their angle intervals
        angle_intervals = []
        for l in range(1, cls.numh):
            h = (k + l) % cls.numh

            # Compute the corner coordinates of other house
            h1corners = cls.compute_corners_of_house(h)

            # Compute angle of each corner (-pi, pi]
            angles = [None]*4
            for i in range(4):
                angles[i] = cls.compute_Xaxis_angle(p0, h1corners[i])

            # Angle interval (initial and maybe final)
            angle_min = min(angles); angle_max = max(angles)

            # If house crosses pi, partition its angle interval
            if angle_max - angle_min > np.pi:
                # Change angles from [-pi, pi) to [0, 2pi)
                for i in range(4):
                    if angles[i] < 0:
                        angles[i] += 2.0*np.pi
                angle_min = min(angles); angle_max = max(angles)

                # Change angles back to [-pi, pi] and store the intervals
                angle_intervals.append([angle_min, np.pi])
                angle_intervals.append([-np.pi, angle_max - 2.0*np.pi])
            else:
                angle_intervals.append([angle_min, angle_max])

        # Sort the angle intervals with smallest angle_min first
        angle_intervals.sort()

        # Compute the union of the angle intervals
        # Here we call a disjoint angle interval "sector"
        sectors = []
        m = 0; M = len(angle_intervals) - 1
        while m <= M:
            sec = [angle_intervals[m][0], angle_intervals[m][1]]
            n = m + 1
            while n <= M and angle_intervals[n][0] <= sec[1]:
                sec[1] = max(sec[1], angle_intervals[n][1])
                n += 1
            sectors.append(sec)
            m = n

        # Compute the total length of all the sectors
        sector_sum = 0.0
        for sec in sectors:
            sector_sum += sec[1] - sec[0]

        return (2.0*np.pi - sector_sum) / (2.0*np.pi)

    @classmethod
    def compute_total_view(cls, numh0, house_positions0):
        # Load house dimensions
        house_dims = np.load("meshes/house_dimensions.npy")
        cls.lx = house_dims[0]; cls.ly = house_dims[1]

        # Restructure the house positions array if needed
        if numh0 != len(house_positions0):
            cls.house_positions = []
            for i in range(int(len(house_positions0)/3)):
                cls.house_positions.append([house_positions0[3*i], \
                                        house_positions0[3*i + 1], \
                                        house_positions0[3*i + 2]])
        else:
           cls.house_positions = house_positions0

        cls.numh = len(cls.house_positions)
        #vh = cls.compute_view_from_house(0)
        #print("View from house 0 is %f" %vh)

        # Compute view from each house
        view_tot = 0.0
        for h in range(cls.numh):
            vh = cls.compute_view_from_house(h)
            #print("View from house %d is %f" %(h, vh))
            #view_tot += cls.compute_view_from_house(h)
            view_tot += vh

        return view_tot / cls.numh

    @classmethod
    def compute_min_view(cls, numh0, house_positions0):
        # Load house dimensions
        house_dims = np.load("meshes/house_dimensions.npy")
        cls.lx = house_dims[0]; cls.ly = house_dims[1]

        # Restructure the house positions array if needed
        if numh0 != len(house_positions0):
            cls.house_positions = []
            for i in range(int(len(house_positions0)/3)):
                cls.house_positions.append([house_positions0[3*i], \
                                        house_positions0[3*i + 1], \
                                        house_positions0[3*i + 2]])
        else:
           cls.house_positions = house_positions0

        cls.numh = len(cls.house_positions)
        #vh = cls.compute_view_from_house(0)
        #print("View from house 0 is %f" %vh)

        # Compute view from each house
        view_min = 2.0
        for h in range(cls.numh):
            vh = cls.compute_view_from_house(h)
            #print("View from house %d is %f" %(h, vh))
            view_min = min(view_min, vh)

        return view_min

    # Compute the normalized maximum total hidden sector
    @classmethod
    def compute_max_hidden_sector(cls, house_positions0):
        return 1.0 - cls.compute_min_view(-1, house_positions0)

    @classmethod
    def view_for_optimization(cls, house_positions0):
        #view = -cls.compute_total_view(-1, house_positions0)
        #view = -cls.compute_min_view(-1, house_positions0)
        hidden_sector = cls.compute_max_hidden_sector(house_positions0)
        print("Maximum hidden sector is", hidden_sector)

        return hidden_sector
