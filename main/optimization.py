import random as rm
import sys
from multimesh_stokes_2D import mumss2D
from view_2D import view2D
from polygon_comp_2D import polygon_comp2D
import scipy.optimize
import numpy as np
from dolfin import *

# Initialize island polygon for "stay on island" constraints
polygon = np.load("meshes/island_boundary_cc.npy")
polygon_comp2D(polygon)

# Compute the dimensions of the air mesh
mumss2D.load_air_mesh()
x_amin = mumss2D.x_amin; x_amax = mumss2D.x_amax
y_amin = mumss2D.y_amin; y_amax = mumss2D.y_amax
xd = x_amax - x_amin
yd = y_amax - y_amin

# Compute the max radius r of a house
house_dims = np.load("meshes/house_dimensions.npy")
r = 0.5*sqrt(house_dims[0]**2 + house_dims[1]**2)

# Compute the max radius R of a house box mesh
mesh_hb = Mesh("meshes/mesh_hb_init.xml")
xs_hb = mesh_hb.coordinates()
xmin = min([x[0] for x in xs_hb])
xmax = max([x[0] for x in xs_hb])
ymin = min([x[1] for x in xs_hb])
ymax = max([x[1] for x in xs_hb])
R = 0.5*sqrt((xmax - xmin)**2 + (ymax - ymin)**2)

# Define "keep distance between houses" constraints (USED IN PENALTY FUNCTION)
def keep_distance_between_houses_penalty(dx, dy):
    d_sqrd = dx**2 + dy**2
    g = d_sqrd - (2.0*R)**2

    ReQU = min(0, g)**2

    return ReQU

# Define "stay on island" constraints (USED IN PENALTY FUNCTION)
def stay_on_island_penalty(x, y):
    d_sqrd = polygon_comp2D.compute_min_sbd(np.array([x, y]))
    g = d_sqrd - r**2

    ReQU = min(0, g)**2

    return ReQU

# Define penalty function
def penalty_function(hps0, numh):
    value_kdbh = 0.0; value_soi = 0.0
    pnlt_param_kdbh = 1e3; pnlt_param_soi = 1e8

    # "Keep distance between houses" contribution
    for i in range(numh - 1):
        for j in range(i + 1, numh):
            dx = hps0[3*i] - hps0[3*j]
            dy = hps0[3*i + 1] - hps0[3*j + 1]
            value_kdbh += keep_distance_between_houses_penalty(dx, dy)

    # "Stay on island" contribution
    for i in range(numh):
        x = hps0[3*i]
        y = hps0[3*i + 1]
        value_soi += stay_on_island_penalty(x, y)

    #print(value_kdbh, value_soi)
    return pnlt_param_kdbh*value_kdbh + pnlt_param_soi*value_soi

# FIXME! This method does not automatically generate feasible points
# For that, see method below this one
def random_house_positions_optimization_columns(numh):
    house_positions = []
    for h in range(numh):
        extra_dist = 0.05*xd
        xmin = 0.1*xd; xmax = 0.9*xd
        ymin = 0.1*yd; ymax = 0.9*yd
        xdiff = xmax - xmin; ydiff = ymax - ymin;
        xrestmin = h * xdiff / numh + extra_dist
        xrestmax = (h + 1 - numh) * xdiff / numh - extra_dist
        yrestmin = 0.0; yrestmax = 0.0
        xmin += xrestmin; xmax += xrestmax
        ymin += yrestmin; ymax += yrestmax
        house_positions.append(rm.uniform(xmin, xmax))
        house_positions.append(rm.uniform(ymin, ymax))
        house_positions.append(rm.uniform(0, 360))

    return house_positions

def random_house_positions_optimization_freely(numh):
    house_positions = []
    for h in range(numh):
        point_not_ok = True # Just to initialize while loop below
        while point_not_ok:
            # Compute random point
            x = rm.uniform(x_amin, x_amax)
            y = rm.uniform(y_amin, y_amax)

            # Assumes point is OK
            point_not_ok = False

            # Check if point is NOT on island
            if stay_on_island_penalty(x, y) > 0.0:
                point_not_ok = True
            else:
                # If point on island, then check
                # if point is close to already added points
                for i in range(h):
                    dx = x - house_positions[3*i]
                    dy = y - house_positions[3*i + 1]
                    if keep_distance_between_houses_penalty(dx, dy) > 0.0:
                        point_not_ok = True
                        break

        house_positions.append(x)
        house_positions.append(y)
        house_positions.append(rm.uniform(0, 360))

    return house_positions

def seven_house_positions_opt_hexagon(R = 0.08549942522316488):
    house_positions = []
    radius = 2.0*R
    center = [0.56, 0.32]
    house_positions.append(center[0])
    house_positions.append(center[1])
    house_positions.append(0.0)
    for h in range(6):
        angle = h*pi/3
        x = center[0] + radius*cos(angle)
        y = center[1] + radius*sin(angle)
        house_positions.append(x)
        house_positions.append(y)
        house_positions.append(0.0)

    return house_positions

def seven_house_positions_opt_horizontal_line(dist = 0.117465586529):
    house_positions = []
    hp0 = [0.128, 0.42, 90.0]
    house_positions.append(hp0[0])
    house_positions.append(hp0[1])
    house_positions.append(hp0[2])
    for h in range(1, 7):
        house_positions.append(hp0[0] + h*dist)
        house_positions.append(hp0[1])
        house_positions.append(hp0[2])

    return house_positions

def seven_house_positions_opt_vertical_line(dist = 0.0709153870112):
    house_positions = []
    hp0 = [0.62, 0.08, 0.0]
    house_positions.append(hp0[0])
    house_positions.append(hp0[1])
    house_positions.append(hp0[2])
    for h in range(1, 7):
        house_positions.append(hp0[0])
        house_positions.append(hp0[1] + h*dist)
        house_positions.append(hp0[2])

    return house_positions

def seven_house_positions_opt_principal_line():
    p0 = np.array([0.17, 0.51])
    p1 = np.array([0.76, 0.24])
    v = p1 - p0
    angle = 90.0 - (180.0/pi)*atan(abs(v[1]/v[0]))
    house_positions = []
    house_positions.append(p0[0])
    house_positions.append(p0[1])
    house_positions.append(angle)
    for h in range(1, 7):
        house_positions.append(p0[0] + h/6.0*v[0])
        house_positions.append(p0[1] + h/6.0*v[1])
        house_positions.append(angle)

    return house_positions

def seven_house_positions_opt_coastal():
    house_positions = []

    # House 0
    house_positions.append(0.25)
    house_positions.append(0.55)
    house_positions.append(6.0)

    # House 1
    house_positions.append(0.51)
    house_positions.append(0.53)
    house_positions.append(-17.0)

    # House 2
    house_positions.append(0.75)
    house_positions.append(0.45)
    house_positions.append(-25)

    # House 3
    house_positions.append(0.78)
    house_positions.append(0.24)
    house_positions.append(-115.0)

    # House 4
    house_positions.append(0.62)
    house_positions.append(0.1)
    house_positions.append(0.0)

    # House 5
    house_positions.append(0.39)
    house_positions.append(0.21)
    house_positions.append(-30.0)

    # House 6
    house_positions.append(0.17)
    house_positions.append(0.39)
    house_positions.append(-41.0)

    return house_positions
def run_optimization(weights, wind_in, config_init, prefix = "recent", numh = 7):

    print("From run_optimization in optimization.py:")

    # Define initial positions for seven houses
    if config_init == "hexagon":
        hps0 = seven_house_positions_opt_hexagon()
    elif config_init == "principal_line":
        hps0 = seven_house_positions_opt_principal_line()
    elif config_init == "coastal":
        hps0 = seven_house_positions_opt_coastal()
    else:
        config_init = "random"
        hps0 = random_house_positions_optimization_freely(numh)

    # Print initial positions of house(s)
    print("Initial positions are:")
    print(hps0)

    # Define objective function
    def objective_function(hps0):
        value = 0.0
        if weights[0] != 0.0:
            value += weights[0]*mumss2D.velocity_for_optimization(hps0, wind_in)
        if weights[1] != 0.0:
            value += weights[1]*view2D.view_for_optimization(hps0)
        value += penalty_function(hps0, numh)
        return value

    # Start optimization
    hps1 = scipy.optimize.minimize(objective_function, hps0, method = 'Powell', options={'maxfev':100000})

    # Print optimization results
    print(hps1)

    # Save the results from optimization
    mumss2D.init_outfiles(numh, prefix)
    mumss2D.place_and_save_house_meshes(hps1.x)
    if weights[0] != 0.0:
        mumss2D.save_solution()

    return hps1
