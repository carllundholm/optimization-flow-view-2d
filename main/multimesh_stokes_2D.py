from dolfin import *

class mumss2D:

    # Create files for storing meshes and solutions
    @classmethod
    def init_outfiles(cls, numh, prefix):
        cls.outfile_mesh_house = []
        cls.outfile_mesh_housebox = []
        cls.outfile_u_air = File("results/"+prefix+"/solutions/u_air.pvd")
        cls.outfile_p_air = File("results/"+prefix+"/solutions/p_air.pvd")
        cls.outfile_house_boundary_markers = []
        cls.outfile_house_markers = []
        cls.outfile_u_hb = []
        cls.outfile_p_hb = []
        for hb in range(numh):
            cls.outfile_mesh_house.append(File("results/"+prefix+"/meshes_pvd/mesh_house"+str(hb)+".pvd"))
            cls.outfile_mesh_housebox.append(File("results/"+prefix+"/meshes_pvd/mesh_hb"+str(hb)+".pvd"))
            cls.outfile_house_boundary_markers.append(File("results/"+prefix+"/meshes_pvd/house_boundary_markers"+str(hb)+".pvd"))
            cls.outfile_house_markers.append(File("results/"+prefix+"/meshes_pvd/house_markers"+str(hb)+".pvd"))
            cls.outfile_u_hb.append(File("results/"+prefix+"/solutions/u_hb"+str(hb)+".pvd"))
            cls.outfile_p_hb.append(File("results/"+prefix+"/solutions/p_hb"+str(hb)+".pvd"))

    # Load background air mesh
    @classmethod
    def load_air_mesh(cls):
        cls.mesh_air = Mesh("meshes/mesh_air.xml")
        File("results/meshes_pvd/mesh_air.pvd") << cls.mesh_air

        # Get air mesh min and max coordinates
        xs = cls.mesh_air.coordinates()
        cls.x_amin = min([x[0] for x in xs])
        cls.x_amax = max([x[0] for x in xs])
        cls.y_amin = min([x[1] for x in xs])
        cls.y_amax = max([x[1] for x in xs])

    # Place house meshes for visualization
    @classmethod
    def place_house_meshes(cls, house_positions):
        # Place houses
        numh = len(house_positions)
        cls.mesh_house = []
        for hb in range(numh):
            mesh_h = Mesh("meshes/mesh_house_init.xml")

            # Rotate house
            angle = house_positions[hb][2]
            mesh_h.rotate(angle, 2)

            # Translate house
            trans_point = Point(house_positions[hb][0], house_positions[hb][1])
            mesh_h.translate(trans_point)

            # Add to list
            cls.mesh_house.append(mesh_h)

    # Place house box meshes and create multimesh hierarchy
    @classmethod
    def place_housebox_meshes(cls, house_positions):
        # Initialize multimesh and first add background air mesh
        cls.multimesh = MultiMesh()
        cls.multimesh.add(cls.mesh_air)

        # Place houses
        cls.numh = len(house_positions)
        cls.house_positions = house_positions
        cls.mesh_housebox = []
        for hb in range(cls.numh):
            mesh_hb = Mesh("meshes/mesh_hb_init.xml")

            # Rotate house
            angle = cls.house_positions[hb][2]
            mesh_hb.rotate(angle, 2)

            # Translate house
            trans_point = Point(cls.house_positions[hb][0], cls.house_positions[hb][1])
            mesh_hb.translate(trans_point)

            # Add to multimesh hierarchy
            cls.mesh_housebox.append(mesh_hb)
            cls.multimesh.add(mesh_hb)

        # Build multimesh hierarchy
        cls.multimesh.build()

    # Save house meshes to file
    @classmethod
    def save_house_meshes(cls):
        for h in range(cls.numh):
            cls.outfile_mesh_house[h] << cls.mesh_house[h]

    # Save house box meshes to file
    @classmethod
    def save_housebox_meshes(cls):
        for hb in range(cls.numh):
            cls.outfile_mesh_housebox[hb] << cls.mesh_housebox[hb]

    # Clear multimesh hierarchy
    @classmethod
    def clear_meshes(cls):
        cls.multimesh.clear()

    # Set the predefined problem data
    @classmethod
    def set_problem_data(cls, vel_in = [2.0/sqrt(5.0), 1.0/sqrt(5.0)]):
        # Define source term
        f = Constant((0.0, 0.0))

        # Define boundary regions
        class XminBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[0], cls.x_amin)

        class XmaxBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[0], cls.x_amax)

        class YBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (near(x[0], cls.x_amin) or
                                        near(x[0], cls.x_amax))

        class YminBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[1], cls.y_amin)

        class YmaxBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[1], cls.y_amax)

        class XBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and (near(x[1], cls.y_amin) or
                                        near(x[1], cls.y_amax))

        ### Define FE-variational formulation ###
        # Create function space
        P2 = VectorElement("P", triangle, 2)
        P1 = FiniteElement("P", triangle, 1)
        TH = P2 * P1
        cls.W = MultiMeshFunctionSpace(cls.multimesh, TH)

        # Define trial and test functions
        (u, p) = TrialFunctions(cls.W)
        (v, q) = TestFunctions(cls.W)

        # Define normal and mesh size
        n = FacetNormal(cls.multimesh)
        h = 2.0 * Circumradius(cls.multimesh)

        # Parameters and functions for bilinear form
        alpha = Constant(1e1)
        beta = Constant(1e0)
        gamma = Constant(1e1)

        def tensor_jump(v, n):
            return outer(v('+'), n('+')) + outer(v('-'), n('-'))

        def a_h(v, w):
            return inner(grad(v), grad(w)) * dX \
                 - inner(avg(grad(v)), tensor_jump(w, n)) * dI \
                 - inner(avg(grad(w)), tensor_jump(v, n)) * dI \
                 + alpha/avg(h) * inner(jump(v), jump(w)) * dI

        def b_h(v, q):
            return -div(v) * q * dX + jump(v, n) * avg(q) * dI

        #def s_O(v, w):
        #    return beta * inner(jump(grad(v)), jump(grad(w))) * dO

        def s_O(v, w):
            return (beta / avg(h)**2) * inner(jump(v), jump(w)) * dO

        def s_P(q, r):
            return gamma * inner(jump(q), jump(r)) * dO

        def s_C(v, q, w, r):
            return h*h * inner(div(grad(v)) - grad(q), div(grad(w)) + grad(r)) * dC

        # Functions for linear form
        def l_h(f, v):
            return inner(f, v) * dX

        def l_C(f, v, q):
            return -h*h * inner(f, div(grad(v)) + grad(q)) * dC

        # Define bilinear form
        a = a_h(u, v) + b_h(u, q) + b_h(v, p) + s_O(u, v) + s_C(u, p, v, q)
        #a = a_h(u, v) + b_h(u, q) + b_h(v, p) + s_O(u, v) + s_C(u, p, v, q) + s_P(p, q)

        # Define linear form
        L =  l_h(f, v) + l_C(f, v, q)

        # Assemble linear system
        cls.A = assemble_multimesh(a)
        cls.b = assemble_multimesh(L)

        # Define boundary values
        cls.inflow_value = Constant((vel_in[0], vel_in[1]))
        cls.outflow_value = Constant(0.0)
        cls.noslip_value = Constant((0.0, 0.0))

        # Create subdomains for boundary conditions
        xmin_boundary = XminBoundary()
        xmax_boundary = XmaxBoundary()
        ymin_boundary = YminBoundary()
        ymax_boundary = YmaxBoundary()

        # Create subspaces for boundary conditions
        cls.V = MultiMeshSubSpace(cls.W, 0)
        cls.Q = MultiMeshSubSpace(cls.W, 1)

        # Create boundary conditions
        bc_xmin = MultiMeshDirichletBC(cls.V, cls.inflow_value, xmin_boundary)
        bc_ymin = MultiMeshDirichletBC(cls.V, cls.inflow_value, ymin_boundary)
        if vel_in[0] != 0.0:
            bc_xmax = MultiMeshDirichletBC(cls.Q, cls.outflow_value, xmax_boundary)
            bc_ymax = MultiMeshDirichletBC(cls.V, cls.inflow_value, ymax_boundary)
        else:
            bc_xmax = MultiMeshDirichletBC(cls.V, cls.inflow_value, xmax_boundary)
            bc_ymax = MultiMeshDirichletBC(cls.Q, cls.outflow_value, ymax_boundary)

        # Apply boundary conditions
        bc_xmin.apply(cls.A, cls.b)
        bc_xmax.apply(cls.A, cls.b)
        bc_ymin.apply(cls.A, cls.b)
        bc_ymax.apply(cls.A, cls.b)

        # Set noslip on the house boundaries and mark background cells in house holes
        for hb in range(cls.numh):
            ff = MeshFunction("size_t", cls.mesh_housebox[hb], "meshes/house_boundary_markers.xml")
            #cls.outfile_house_boundary_markers[hb] << ff
            house_boundary_noslip = MultiMeshDirichletBC(cls.V, cls.noslip_value, ff, 1, hb + 1)
            house_boundary_noslip.apply(cls.A, cls.b)

            hole_midpoint = Point(cls.house_positions[hb][0], cls.house_positions[hb][1])
            cls.multimesh.auto_cover(0, hole_midpoint)

        # Set inactive dofs to zero
        cls.W.lock_inactive_dofs(cls.A, cls.b)

    @classmethod
    def solve_problem(cls):
        # Compute solution
        cls.w = MultiMeshFunction(cls.W)
        solve(cls.A, cls.w.vector(), cls.b)

    @classmethod
    def compute_maxspeed_component(cls):
        # Compute solution
        cls.solve_problem()

        # Compute max speed component
        maxspeedcomp = 0.0
        for p in range(1 + cls.numh):
            speedcomp = max(abs(cls.w.part(p).sub(0).compute_vertex_values()))
            if speedcomp > maxspeedcomp:
                maxspeedcomp = speedcomp

        return maxspeedcomp

    @classmethod
    def compute_Lp_norm_house_velocity(cls, p):
        # Compute solution
        cls.solve_problem()

        norm_max = 0.0
        for hb in range(cls.numh):
            u_hb = cls.w.part(hb + 1).sub(0)
            Lp_norm_uhb = assemble((u_hb**2)**(p/2)*dx)**(1/p)
            norm_max = max(Lp_norm_uhb, norm_max)

        return norm_max


    @classmethod
    def save_solution(cls):
        # Set noslip on the house(s) for post-solving visualisation purposes
        #for hb in range(cls.numh):
            #ff = MeshFunction("size_t", cls.mesh_housebox[hb], "meshes/house_markers.xml")
            #cls.outfile_house_markers[hb] << ff
            #house_noslip = MultiMeshDimumsrichletBC(cls.V, cls.noslip_value, ff, 1, hb + 1)
            #house_noslip.apply(cls.w.vector())

        # Save solution to file
        u_air = cls.w.part(0).sub(0)
        p_air = cls.w.part(0).sub(1)
        cls.outfile_u_air << u_air
        cls.outfile_p_air << p_air

        for hb in range(cls.numh):
            u_hb = cls.w.part(hb + 1).sub(0)
            p_hb = cls.w.part(hb + 1).sub(1)

            cls.outfile_u_hb[hb] << u_hb
            cls.outfile_p_hb[hb] << p_hb

    @classmethod
    def run_mumss2D(cls, house_positions):
        # Set house meshes
        cls.place_housebox_meshes(house_positions)
        print("Background mesh set")
        print(cls.numh, "additional overlapping meshes set")

        # Save house meshes
        #cls.save_housebox_meshes()

        # Define problem
        print("Setting problem data ...")
        cls.set_problem_data()
        print("Problem data set")

        # Solve problem
        print("Solving problem ...")
        cls.solve_problem()
        print("Problem solved")

        # Save solution
        cls.save_solution()

        # Clear multimesh hierarchy
        cls.clear_meshes()
        print("multimesh hierarchy cleared")

    @classmethod
    def debug_mumss2D(cls, house_positions, criticalspeed):
        # Set house meshes
        cls.place_housebox_meshes(house_positions)

        # Define problem
        cls.set_problem_data()

        # Solve problem and compute max speed component
        maxspeedcomp = cls.compute_maxspeed_component()
        maxspeed = maxspeedcomp
        print("Max speed test = ", maxspeed)

        # Check if critical speed has been reached
        if  maxspeed > criticalspeed: #critical_speed:
            print("WARNING! Max speed is above critical speed of", criticalspeed)

            # Save house meshes
            #cls.save_house_meshes()

            # Save solution
            cls.save_solution()

            return False

        # Clear multimesh hierarchy
        cls.clear_meshes()

        return True

    @classmethod
    def velocity_for_optimization(cls, house_positions0, wind_in = [2.0/sqrt(5.0), 1.0/sqrt(5.0)]):
        if len(house_positions0) % 3 != 0:
            print("ERROR! Input house positions in wrong format")
            return None

        # Restructure the house positions array
        house_positions = []
        for i in range(int(len(house_positions0)/3)):
            house_positions.append([house_positions0[3*i], \
                                    house_positions0[3*i + 1], \
                                    house_positions0[3*i + 2]])

        #print(house_positions)

        # Place house box meshes on top of the background mesh
        cls.place_housebox_meshes(house_positions)

        # Set predefined problem data
        cls.set_problem_data(wind_in)

        # Solve problem and compute velocity used for optimization
        p_norm = 100
        norm_max = cls.compute_Lp_norm_house_velocity(p_norm)
        print("The largest L%d-norm of house velocities = %f" %(p_norm, norm_max))

        # Clear multimesh hierarchy
        cls.clear_meshes()

        return norm_max

    @classmethod
    def place_and_save_house_meshes(cls, house_positions0):
        if len(house_positions0) % 3 != 0:
            print("ERROR! Input house positions in wrong format")
            return None

        # Restructure the house positions array
        house_positions = []
        for i in range(int(len(house_positions0)/3)):
            house_positions.append([house_positions0[3*i], \
                                    house_positions0[3*i + 1], \
                                    house_positions0[3*i + 2]])


        # Place house and house box meshes on top of the background mesh
        cls.place_house_meshes(house_positions)
        cls.place_housebox_meshes(house_positions)

        # Save house and house box meshes
        cls.save_house_meshes()
        cls.save_housebox_meshes()

        # Clear multimesh hierarchy
        cls.clear_meshes()

        return None
