#!/usr/bin/env bash

echo "Would like to start several optimization computations?"
echo "Press [1] for PURE WIND optimization with PREDETERMINED initial configurations"
echo "Press [2] for PURE WIND optimization with RANDOM initial configurations"
echo "Press [3] for MIXED optimization with PREDETERMINED initial configurations"
echo "Press [4] for MIXED optimization with RANDOM initial configurations"
echo "Press [5] for PURE VIEW optimization with PREDETERMINED initial configurations"
echo "Press [6] for PURE VIEW optimization with RANDOM initial configurations"
echo "Press any other key to abort."
read choice

if [ $choice -eq 1 ]; then
    mkdir logs
    echo "Computing pure wind opt results with predetermined initial configurations"
    WEIGHT_WIND=1.0
    for WIND_TYPE in horizontal diagonal vertical; do
	for CONFIG_INIT in hexagon principal_line coastal; do
	    PREFIX="opt_purewind_"$WIND_TYPE"_"$CONFIG_INIT
	    echo "Running" $PREFIX
	    python3 main.py $WEIGHT_WIND $WIND_TYPE $CONFIG_INIT $PREFIX >& "logs/"$PREFIX".log"
	done
    done
elif [ $choice -eq 2 ]; then
    mkdir logs
    echo "Computing pure wind opt results with random initial configurations"
    WEIGHT_WIND=1.0
    CONFIG_INIT=random
    for WIND_TYPE in horizontal diagonal vertical; do
	for SIM in 1 2 3; do
	    PREFIX="opt_purewind_"$WIND_TYPE"_"$CONFIG_INIT"_"$SIM
	    echo "Running" $PREFIX
	    python3 main.py $WEIGHT_WIND $WIND_TYPE $CONFIG_INIT $PREFIX >& "logs/"$PREFIX".log"
	done
    done
elif [ $choice -eq 3 ]; then
    mkdir logs
    echo "Computing mixed wind opt results with predetermined initial configurations"
    WIND_TYPE=diagonal
    for WEIGHT_WIND in 0.9 0.7 0.5 0.3 0.1; do
	for CONFIG_INIT in hexagon principal_line coastal; do
	    PREFIX="opt_mixed_ww="$WEIGHT_WIND"_"$WIND_TYPE"_"$CONFIG_INIT
	    echo "Running" $PREFIX
	    python3 main.py $WEIGHT_WIND $WIND_TYPE $CONFIG_INIT $PREFIX >& "logs/"$PREFIX".log" 
	done
    done
elif [ $choice -eq 4 ]; then
    mkdir logs
    echo "Computing mixed wind opt results with random initial configurations"
    WIND_TYPE=diagonal
    CONFIG_INIT=random
    for WEIGHT_WIND in 0.9 0.7 0.5 0.3 0.1; do
    	for SIM in 1 2 3; do
	    PREFIX="opt_mixed_ww="$WEIGHT_WIND"_"$WIND_TYPE"_"$CONFIG_INIT"_"$SIM
	    echo "Running" $PREFIX
	    python3 main.py $WEIGHT_WIND $WIND_TYPE $CONFIG_INIT $PREFIX >& "logs/"$PREFIX".log"
        done
    done
elif [ $choice -eq 5 ]; then
    mkdir logs
    echo "Computing pure view opt results with predetermined initial configurations"
    WEIGHT_WIND=0.0
    WIND_TYPE=diagonal
    for CONFIG_INIT in hexagon principal_line coastal; do
        PREFIX="opt_pureview_"$CONFIG_INIT
	echo "Running" $PREFIX
	python3 main.py $WEIGHT_WIND $WIND_TYPE $CONFIG_INIT $PREFIX >& "logs/"$PREFIX".log"
    done
elif [ $choice -eq 6 ]; then
    mkdir logs
    echo "Computing pure view opt results with random initial configurations"
    WEIGHT_WIND=0.0
    WIND_TYPE=diagonal
    CONFIG_INIT=random
    for SIM in {1..9}; do
	PREFIX="opt_pureview_"$CONFIG_INIT"_"$SIM
	echo "Running" $PREFIX
	python3 main.py $WEIGHT_WIND $WIND_TYPE $CONFIG_INIT $PREFIX >& "logs/"$PREFIX".log"
    done
fi
