import numpy as np

class polygon_comp2D:

    @classmethod
    def __init__(cls, polygon):
        cls.polygon = polygon
        cls.poly_len = len(cls.polygon)

    # Compute minimum squared boundary distance
    @classmethod
    def compute_min_sbd(cls, q):

        # Loop over all points in polygon to compute smallest distance
        # from point q to the boundary of polygon
        dist_min = 1e10 # Max distance in mesh normalized to length 1.0
        in_segment_min = None
        k_min = -1
        for k in range(cls.poly_len):

            # Compute the orthogonal projection of q onto segment k
            p0 = cls.polygon[k]; p1 = cls.polygon[(k + 1) % cls.poly_len]
            Q = q - p0; P = p1 - p0
            q_proj = p0 + (np.dot(Q, P) / np.dot(P, P))*P

            # Check if the projection lies in the segment
            if np.dot((q_proj - p0), (q_proj - p1)) <= 0.0:
                in_segment = True
                R = q - q_proj
                dist = np.dot(R, R)
            else:
                in_segment = False
                v1 = q - p1
                dist0 = np.dot(Q, Q); dist = np.dot(v1, v1)
                if dist0 < dist:
                    dist = dist0
                else:
                    k = (k + 1) % cls.poly_len

            # Use possibly smaller new min squared distance
            if dist <= dist_min:
                dist_min = dist
                in_segment_min = in_segment
                q_proj_min = q_proj
                k_min = k

        # Check if q lies inside the polygon
        # If it doesn't, then make min distance negative
        if in_segment_min:
            P = cls.polygon[(k_min + 1) % cls.poly_len] - cls.polygon[k_min]
            R = q - q_proj_min
            if np.cross(P, R) <= 0.0:
                dist_min = -dist_min
        else:
            A = cls.polygon[k_min] - cls.polygon[(k_min - 1) % cls.poly_len]
            B = cls.polygon[(k_min + 1) % cls.poly_len] - cls.polygon[k_min]
            if np.cross(A, B) >= 0.0:
                dist_min = -dist_min

        return dist_min
