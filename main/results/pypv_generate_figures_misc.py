#!/usr/bin/pvpython

from paraview.simple import *
import os

# FIXME: Get path to where file is stored (in a peasant way)
path = '/Users/carlun/dev/fenics/projects/mumss/'
path += 'optimization-flow-view-2d/main/results/'

### Set up the air mesh and view size nicely ###
mesh_air = 'meshes_pvd/mesh_air.pvd'

reader_air = OpenDataFile(path + mesh_air)

# Set surface color
dp = GetDisplayProperties()
cn = 255.
dp.DiffuseColor = [85/cn, 170/cn, 255/cn]

# Compute data bounds for view size
Render()
bounds = reader_air.GetDataInformation().GetBounds()
bounds_dx = abs(bounds[1] - bounds[0])
bounds_dy = abs(bounds[3] - bounds[2])

aspect = bounds_dy/bounds_dx
width = 1024
height = int(width*aspect)

view = GetRenderView()
view.CenterAxesVisibility = 0
view.OrientationAxesVisibility = 0
view.ViewSize = [width, height]
Render()
view.ResetCamera()

# For fine tuning
config_camZoom = 1.8
cam = GetActiveCamera()
cam.Zoom(config_camZoom)

### Set up the island mesh nicely ###
mesh_island = 'meshes_pvd/mesh_island.pvd'

reader_island = OpenDataFile(path + mesh_island)

#set surface color
dp = GetDisplayProperties()
dp.DiffuseColor = [85/cn, 255/cn, 127/cn]

### Set up houses nicely
setting = "misc"
setting_path = path + setting + "/"

# Check if directory for figures needs to be made
figure_dir_path = path + "figures/" + setting + "/"
if not os.path.exists(figure_dir_path):
    os.makedirs(figure_dir_path)

# Loop over the different cases in one setting
cases = [f for f in os.listdir(setting_path) if "random" in f]
print(cases)
for case in cases:
    case_path = setting_path + case + "/"

    print(case_path)

    # Loop over houses in one case and add them to paraview
    reader_houses = []
    for h in range(0, 7):
        house_mesh = case_path + "meshes_pvd/mesh_house"+ str(h) + ".pvd"
        reader_houses.append(OpenDataFile(house_mesh))
        dp = GetDisplayProperties()
        dp.DiffuseColor = [1, 0, 0]

    # Save the screenshot
    SaveScreenshot(figure_dir_path + case + ".png")

    # Delete houses in paraview
    for r in reader_houses:
        Delete(r)



# Delete air and island in paraview
Delete(reader_air)
Delete(reader_island)
