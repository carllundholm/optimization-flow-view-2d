#!/usr/bin/pvpython

from paraview.simple import *
import os

# FIXME: Get path to where file is stored (in a peasant way)
path = '/Users/carlun/dev/fenics/projects/mumss/'
path += 'optimization-flow-view-2d/main/results/'

### Set up glyphs of flow solutions nicely

def MakeNiceGlyphs(data):

    # Make glyphs for the background flow
    glyph = Glyph(data)
    glyph.ScaleMode = "vector"
    glyph.ScaleFactor = 0.05

    dp = GetDisplayProperties(glyph)
    dp.LookupTable = MakeBlueToRedLT(0.0, 2.0)
    dp.ColorArrayName = "GlyphVector"

    return glyph

# Loop over optimization settings (No pureview since they don't have flows)
opt_settings = [f for f in os.listdir(path) if "opt" in f and "view" not in f]
for opt_setting in opt_settings:
    opt_setting_path = path + opt_setting + "/"

    # Check if directory for figures needs to be made
    figure_dir_path = path + "figures/flows_glyphs/" + opt_setting + "/"
    if not os.path.exists(figure_dir_path):
        os.makedirs(figure_dir_path)

    # Loop over the different cases in one setting
    cases = [f for f in os.listdir(opt_setting_path) if "opt" in f]
    for case in cases:
        case_path = opt_setting_path + case + "/"

        # Set up the background flow
        air_flow = case_path + "solutions/u_air.pvd"
        reader_air = OpenDataFile(air_flow)
        dp = GetDisplayProperties(reader_air)
        dp.Representation = "Outline"

        glyph_air = MakeNiceGlyphs(reader_air)

        # Compute data bounds for view size
        bounds = reader_air.GetDataInformation().GetBounds()
        bounds_dx = abs(bounds[1] - bounds[0])
        bounds_dy = abs(bounds[3] - bounds[2])

        aspect = bounds_dy/bounds_dx
        width = 1024
        height = int(width*aspect)

        view = GetRenderView()
        view.CenterAxesVisibility = 0
        view.OrientationAxesVisibility = 0
        view.ViewSize = [width, height]
        view.ResetCamera()

        # For fine tuning
        config_camZoom = 1.8
        cam = GetActiveCamera()
        cam.Zoom(config_camZoom)

        # Add the island for visualization
        reader_island = OpenDataFile(path + "meshes_pvd/mesh_island.pvd")
        dp = GetDisplayProperties()
        dp.Representation = "Surface"

        # Loop over houses and their surrounding flows in one case
        reader_meshes = []
        reader_flows = []
        reader_glyphs = []
        for h in range(0, 7):
            house_mesh = case_path + "meshes_pvd/mesh_house"+ str(h) + ".pvd"
            reader_meshes.append(OpenDataFile(house_mesh))
            dp = GetDisplayProperties(reader_meshes[h])
            dp.DiffuseColor = [1, 0, 0]

            house_flow = case_path + "solutions/u_hb"+ str(h) + ".pvd"
            reader_flows.append(OpenDataFile(house_flow))
            #reader_glyphs.append(MakeNiceGlyphs(reader_flows[h]))

        # Save the screenshot
        SaveScreenshot(figure_dir_path + case + ".png")

        # Delete air in paraview
        Delete(reader_air)
        Delete(reader_island)
        Delete(glyph_air)

        # Delete houses in paraview
        for h in range(0, 7):
            Delete(reader_meshes[h])
            Delete(reader_flows[h])
            #Delete(reader_glyphs[h])
