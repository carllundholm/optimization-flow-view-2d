from multimesh_stokes_2D import mumss2D
from view_2D import view2D
from optimization import *

numh = 7
config_inits = ["hexagon", "principal_line", "coastal", "single", "random"]
for config_init in config_inits:
    prefix = "misc/initial_position_"+config_init
    if config_init == "hexagon":
        hps0 = seven_house_positions_opt_hexagon()
    elif config_init == "principal_line":
        hps0 = seven_house_positions_opt_principal_line()
    elif config_init == "coastal":
        hps0 = seven_house_positions_opt_coastal()
    elif config_init == "single":
        hps0 = seven_house_positions_opt_hexagon()
        numh = 1
        hps0 = hps0[0:3]
    else:
        config_init = "random"
        hps0 = random_house_positions_optimization_freely(numh)

    print(config_init)

    # Compute view for optimization
    view2D.view_for_optimization(hps0)

    # Compute velocity for optimization
    mumss2D.velocity_for_optimization(hps0)

    # Save the results
    mumss2D.init_outfiles(numh, prefix)
    mumss2D.place_and_save_house_meshes(hps0)
    mumss2D.save_solution()
